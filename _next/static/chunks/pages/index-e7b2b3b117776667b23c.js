_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
  [6],
  {
    "+b/q": function (t, e, a) {
      t.exports = {
        container: "Home_container__38zYD",
        logo_soroban: "Home_logo_soroban__GILaP",
        reload_icon: "Home_reload_icon__22T0W",
        main: "Home_main__37Tob",
        title: "Home_title__21FyB",
        code: "Home_code__2eBcp",
        grid: "Home_grid__1XYpn",
        left_sidebar: "Home_left_sidebar__16iMy",
        active: "Home_active__npnSe",
        box: "Home_box__1b2mL",
        filter: "Home_filter__2wHdd",
        select_box: "Home_select_box__1RsZJ",
        decimal: "Home_decimal__3_qAp",
        name: "Home_name__3qS0v",
        decimal_num: "Home_decimal_num__1JFFZ",
        soroban_color: "Home_soroban_color__3PBHM",
        soroban_num: "Home_soroban_num__3Fufi",
        value: "Home_value__Cz7Wx",
        decimal_name: "Home_decimal_name__1-PJt",
        cta_reload: "Home_cta_reload__2GQru",
        reload_soroban: "Home_reload_soroban__7pE16",
        set_num: "Home_set_num__1HBod",
        set_color: "Home_set_color__zKuZb",
        yellow: "Home_yellow__285tB",
        blue: "Home_blue__2LmhV",
        pink: "Home_pink__dcRUd",
        luc: "Home_luc__3cwNc",
        tia: "Home_tia__2p6PK",
        tim: "Home_tim__3Pg85",
        dark_theme: "Home_dark_theme__JfV3L",
        light_theme: "Home_light_theme__uqbUE",
        menu: "Home_menu__24cCY",
        close_menu: "Home_close_menu__2wZBN",
        footer: "Home_footer__bfZex",
        description: "Home_description__2X-ER",
        card: "Home_card__3AhjP",
        logo: "Home_logo__2IRws",
      };
    },
    "/0+H": function (t, e, a) {
      "use strict";
      (e.__esModule = !0),
        (e.isInAmpMode = o),
        (e.useAmp = function () {
          return o(r.default.useContext(s.AmpStateContext));
        });
      var n,
        r =
          (n = a("q1tI")) && n.__esModule
            ? n
            : {
                default: n,
              },
        s = a("lwAK");

      function o() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
          e = t.ampFirst,
          a = void 0 !== e && e,
          n = t.hybrid,
          r = void 0 !== n && n,
          s = t.hasQuery,
          o = void 0 !== s && s;
        return a || (r && o);
      }
    },
    "/GRZ": function (t, e) {
      t.exports = function (t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
      };
    },
    "/aNb": function (t, e, a) {
      "use strict";
      var n =
          (this && this.__makeTemplateObject) ||
          function (t, e) {
            return (
              Object.defineProperty
                ? Object.defineProperty(t, "raw", {
                    value: e,
                  })
                : (t.raw = e),
              t
            );
          },
        r =
          (this && this.__extends) ||
          (function () {
            var t = function (e, a) {
              return (t =
                Object.setPrototypeOf ||
                ({
                  __proto__: [],
                } instanceof Array &&
                  function (t, e) {
                    t.__proto__ = e;
                  }) ||
                function (t, e) {
                  for (var a in e) e.hasOwnProperty(a) && (t[a] = e[a]);
                })(e, a);
            };
            return function (e, a) {
              function n() {
                this.constructor = e;
              }
              t(e, a), (e.prototype = null === a ? Object.create(a) : ((n.prototype = a.prototype), new n()));
            };
          })(),
        s =
          (this && this.__importStar) ||
          function (t) {
            if (t && t.__esModule) return t;
            var e = {};
            if (null != t) for (var a in t) Object.hasOwnProperty.call(t, a) && (e[a] = t[a]);
            return (e.default = t), e;
          };
      Object.defineProperty(e, "__esModule", {
        value: !0,
      });
      var o,
        c,
        u = s(a("q1tI")),
        l = a("qKvR"),
        i = a("LDyf"),
        h = l.keyframes(
          o ||
            (o = n(
              ["\n  50% {transform: scale(0.75);opacity: 0.2}\n  100% {transform: scale(1);opacity: 1}\n"],
              ["\n  50% {transform: scale(0.75);opacity: 0.2}\n  100% {transform: scale(1);opacity: 1}\n"]
            ))
        ),
        d = (function (t) {
          function e() {
            var e = (null !== t && t.apply(this, arguments)) || this;
            return (
              (e.style = function (t) {
                var a = e.props,
                  r = a.color,
                  s = a.size,
                  o = a.margin;
                return l.css(
                  c ||
                    (c = n(
                      [
                        "\n      display: inline-block;\n      background-color: ",
                        ";\n      width: ",
                        ";\n      height: ",
                        ";\n      margin: ",
                        ";\n      border-radius: 100%;\n      animation: ",
                        " 0.7s ",
                        " infinite linear;\n      animation-fill-mode: both;\n    ",
                      ],
                      [
                        "\n      display: inline-block;\n      background-color: ",
                        ";\n      width: ",
                        ";\n      height: ",
                        ";\n      margin: ",
                        ";\n      border-radius: 100%;\n      animation: ",
                        " 0.7s ",
                        " infinite linear;\n      animation-fill-mode: both;\n    ",
                      ]
                    )),
                  r,
                  i.cssValue(s),
                  i.cssValue(s),
                  i.cssValue(o),
                  h,
                  t % 2 ? "0s" : "0.35s"
                );
              }),
              e
            );
          }
          return (
            r(e, t),
            (e.prototype.render = function () {
              var t = this.props,
                e = t.loading,
                a = t.css;
              return e
                ? l.jsx(
                    "div",
                    {
                      css: [a],
                    },
                    l.jsx("div", {
                      css: this.style(1),
                    }),
                    l.jsx("div", {
                      css: this.style(2),
                    }),
                    l.jsx("div", {
                      css: this.style(3),
                    })
                  )
                : null;
            }),
            (e.defaultProps = i.sizeMarginDefaults(15)),
            e
          );
        })(u.PureComponent);
      e.default = d;
    },
    "1Mdp": function (t, e, a) {
      Object.defineProperty(e, "__esModule", {
        value: !0,
      });
      var n = a("q1tI");

      function r() {
        return (r =
          Object.assign ||
          function (t) {
            for (var e = 1; e < arguments.length; e++) {
              var a = arguments[e];
              for (var n in a) Object.prototype.hasOwnProperty.call(a, n) && (t[n] = a[n]);
            }
            return t;
          }).apply(this, arguments);
      }
      var s = n.createElement(
          "svg",
          {
            viewBox: "-2 -5 14 20",
            height: "100%",
            width: "100%",
            style: {
              position: "absolute",
              top: 0,
            },
          },
          n.createElement("path", {
            d: "M9.9 2.12L7.78 0 4.95 2.828 2.12 0 0 2.12l2.83 2.83L0 7.776 2.123 9.9 4.95 7.07 7.78 9.9 9.9 7.776 7.072 4.95 9.9 2.12",
            fill: "#fff",
            fillRule: "evenodd",
          })
        ),
        o = n.createElement(
          "svg",
          {
            height: "100%",
            width: "100%",
            viewBox: "-2 -5 17 21",
            style: {
              position: "absolute",
              top: 0,
            },
          },
          n.createElement("path", {
            d: "M11.264 0L5.26 6.004 2.103 2.847 0 4.95l5.26 5.26 8.108-8.107L11.264 0",
            fill: "#fff",
            fillRule: "evenodd",
          })
        );

      function c(t) {
        if (7 === t.length) return t;
        for (var e = "#", a = 1; a < 4; a += 1) e += t[a] + t[a];
        return e;
      }

      function u(t, e, a, n, r) {
        return (function (t, e, a, n, r) {
          var s = (t - a) / (e - a);
          if (0 === s) return n;
          if (1 === s) return r;
          for (var o = "#", c = 1; c < 6; c += 2) {
            var u = parseInt(n.substr(c, 2), 16),
              l = parseInt(r.substr(c, 2), 16),
              i = Math.round((1 - s) * u + s * l).toString(16);
            1 === i.length && (i = "0" + i), (o += i);
          }
          return o;
        })(t, e, a, c(n), c(r));
      }
      var l = (function (t) {
        function e(e) {
          t.call(this, e);
          var a = e.height,
            n = e.width,
            r = e.checked;
          (this.t = e.handleDiameter || a - 2),
            (this.i = Math.max(n - a, n - (a + this.t) / 2)),
            (this.o = Math.max(0, (a - this.t) / 2)),
            (this.state = {
              s: r ? this.i : this.o,
            }),
            (this.n = 0),
            (this.e = 0),
            (this.h = this.h.bind(this)),
            (this.r = this.r.bind(this)),
            (this.a = this.a.bind(this)),
            (this.c = this.c.bind(this)),
            (this.l = this.l.bind(this)),
            (this.u = this.u.bind(this)),
            (this.f = this.f.bind(this)),
            (this.p = this.p.bind(this)),
            (this.b = this.b.bind(this)),
            (this.g = this.g.bind(this)),
            (this.v = this.v.bind(this)),
            (this.w = this.w.bind(this));
        }
        return (
          t && (e.__proto__ = t),
          (((e.prototype = Object.create(t && t.prototype)).constructor = e).prototype.componentDidUpdate = function (
            t
          ) {
            t.checked !== this.props.checked &&
              this.setState({
                s: this.props.checked ? this.i : this.o,
              });
          }),
          (e.prototype.k = function (t) {
            this.y.focus(),
              this.setState({
                C: t,
                M: !0,
                m: Date.now(),
              });
          }),
          (e.prototype.x = function (t) {
            var e = this.state,
              a = e.C,
              n = e.s,
              r = (this.props.checked ? this.i : this.o) + t - a;
            e.R ||
              t === a ||
              this.setState({
                R: !0,
              });
            var s = Math.min(this.i, Math.max(this.o, r));
            s !== n &&
              this.setState({
                s: s,
              });
          }),
          (e.prototype.S = function (t) {
            var e = this.state,
              a = e.s,
              n = e.R,
              r = e.m,
              s = this.props.checked,
              o = (this.i + this.o) / 2,
              c = Date.now() - r;
            !n || c < 250
              ? this.T(t)
              : s
              ? o < a
                ? this.setState({
                    s: this.i,
                  })
                : this.T(t)
              : a < o
              ? this.setState({
                  s: this.o,
                })
              : this.T(t),
              this.setState({
                R: !1,
                M: !1,
              }),
              (this.n = Date.now());
          }),
          (e.prototype.h = function (t) {
            t.preventDefault(),
              ("number" == typeof t.button && 0 !== t.button) ||
                (this.k(t.clientX),
                window.addEventListener("mousemove", this.r),
                window.addEventListener("mouseup", this.a));
          }),
          (e.prototype.r = function (t) {
            t.preventDefault(), this.x(t.clientX);
          }),
          (e.prototype.a = function (t) {
            this.S(t), window.removeEventListener("mousemove", this.r), window.removeEventListener("mouseup", this.a);
          }),
          (e.prototype.c = function (t) {
            (this.$ = null), this.k(t.touches[0].clientX);
          }),
          (e.prototype.l = function (t) {
            this.x(t.touches[0].clientX);
          }),
          (e.prototype.u = function (t) {
            t.preventDefault(), this.S(t);
          }),
          (e.prototype.p = function (t) {
            50 < Date.now() - this.n &&
              (this.T(t),
              50 < Date.now() - this.e &&
                this.setState({
                  M: !1,
                }));
          }),
          (e.prototype.b = function () {
            this.e = Date.now();
          }),
          (e.prototype.g = function () {
            this.setState({
              M: !0,
            });
          }),
          (e.prototype.v = function () {
            this.setState({
              M: !1,
            });
          }),
          (e.prototype.w = function (t) {
            this.y = t;
          }),
          (e.prototype.f = function (t) {
            t.preventDefault(),
              this.y.focus(),
              this.T(t),
              this.setState({
                M: !1,
              });
          }),
          (e.prototype.T = function (t) {
            var e = this.props;
            (0, e.onChange)(!e.checked, t, e.id);
          }),
          (e.prototype.render = function () {
            var t = this.props,
              e = t.disabled,
              a = t.className,
              s = t.offColor,
              o = t.onColor,
              c = t.offHandleColor,
              l = t.onHandleColor,
              i = t.checkedIcon,
              h = t.uncheckedIcon,
              d = t.boxShadow,
              b = t.activeBoxShadow,
              f = t.height,
              m = t.width,
              g = (function (t, e) {
                var a = {};
                for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && -1 === e.indexOf(n) && (a[n] = t[n]);
                return a;
              })(t, [
                "disabled",
                "className",
                "offColor",
                "onColor",
                "offHandleColor",
                "onHandleColor",
                "checkedIcon",
                "uncheckedIcon",
                "boxShadow",
                "activeBoxShadow",
                "height",
                "width",
                "handleDiameter",
              ]),
              j = this.state,
              p = j.s,
              x = j.R,
              O = j.M,
              y = {
                position: "relative",
                display: "inline-block",
                textAlign: "left",
                opacity: e ? 0.5 : 1,
                direction: "ltr",
                borderRadius: f / 2,
                WebkitTransition: "opacity 0.25s",
                MozTransition: "opacity 0.25s",
                transition: "opacity 0.25s",
                touchAction: "none",
                WebkitTapHighlightColor: "rgba(0, 0, 0, 0)",
                WebkitUserSelect: "none",
                MozUserSelect: "none",
                msUserSelect: "none",
                userSelect: "none",
              },
              v = {
                height: f,
                width: m,
                margin: Math.max(0, (this.t - f) / 2),
                position: "relative",
                background: u(p, this.i, this.o, s, o),
                borderRadius: f / 2,
                cursor: e ? "default" : "pointer",
                WebkitTransition: x ? null : "background 0.25s",
                MozTransition: x ? null : "background 0.25s",
                transition: x ? null : "background 0.25s",
              },
              M = {
                height: f,
                width: Math.min(1.5 * f, m - (this.t + f) / 2 + 1),
                position: "relative",
                opacity: (p - this.o) / (this.i - this.o),
                pointerEvents: "none",
                WebkitTransition: x ? null : "opacity 0.25s",
                MozTransition: x ? null : "opacity 0.25s",
                transition: x ? null : "opacity 0.25s",
              },
              R = {
                height: f,
                width: Math.min(1.5 * f, m - (this.t + f) / 2 + 1),
                position: "absolute",
                opacity: 1 - (p - this.o) / (this.i - this.o),
                right: 0,
                top: 0,
                pointerEvents: "none",
                WebkitTransition: x ? null : "opacity 0.25s",
                MozTransition: x ? null : "opacity 0.25s",
                transition: x ? null : "opacity 0.25s",
              },
              k = {
                height: this.t,
                width: this.t,
                background: u(p, this.i, this.o, c, l),
                display: "inline-block",
                cursor: e ? "default" : "pointer",
                borderRadius: "50%",
                position: "absolute",
                transform: "translateX(" + p + "px)",
                top: Math.max(0, (f - this.t) / 2),
                outline: 0,
                boxShadow: O ? b : d,
                border: 0,
                WebkitTransition: x ? null : "background-color 0.25s, transform 0.25s, box-shadow 0.15s",
                MozTransition: x ? null : "background-color 0.25s, transform 0.25s, box-shadow 0.15s",
                transition: x ? null : "background-color 0.25s, transform 0.25s, box-shadow 0.15s",
              };
            return n.createElement(
              "div",
              {
                className: a,
                style: y,
              },
              n.createElement(
                "div",
                {
                  className: "react-switch-bg",
                  style: v,
                  onClick: e ? null : this.f,
                  onMouseDown: function (t) {
                    return t.preventDefault();
                  },
                },
                i &&
                  n.createElement(
                    "div",
                    {
                      style: M,
                    },
                    i
                  ),
                h &&
                  n.createElement(
                    "div",
                    {
                      style: R,
                    },
                    h
                  )
              ),
              n.createElement("div", {
                className: "react-switch-handle",
                style: k,
                onClick: function (t) {
                  return t.preventDefault();
                },
                onMouseDown: e ? null : this.h,
                onTouchStart: e ? null : this.c,
                onTouchMove: e ? null : this.l,
                onTouchEnd: e ? null : this.u,
                onTouchCancel: e ? null : this.v,
              }),
              n.createElement(
                "input",
                r(
                  {},
                  {
                    type: "checkbox",
                    role: "switch",
                    disabled: e,
                    style: {
                      border: 0,
                      clip: "rect(0 0 0 0)",
                      height: 1,
                      margin: -1,
                      overflow: "hidden",
                      padding: 0,
                      position: "absolute",
                      width: 1,
                    },
                  },
                  g,
                  {
                    ref: this.w,
                    onFocus: this.g,
                    onBlur: this.v,
                    onKeyUp: this.b,
                    onChange: this.p,
                  }
                )
              )
            );
          }),
          e
        );
      })(n.Component);
      (l.defaultProps = {
        disabled: !1,
        offColor: "#888",
        onColor: "#080",
        offHandleColor: "#fff",
        onHandleColor: "#fff",
        uncheckedIcon: s,
        checkedIcon: o,
        boxShadow: null,
        activeBoxShadow: "0 0 2px 3px #3bf",
        height: 28,
        width: 56,
      }),
        (e.default = l);
    },
    "2GlY": function (t, e) {
      t.exports = "_next/static/images/ban-tinh-07-3859f181b8d6c0adaaa27a0f66f62edd.png";
    },
    "2dTo": function (t, e) {
      t.exports = "_next/static/images/ban-tinh-05-5174e0eca1bad0106add0dbfd79be57f.png";
    },
    "40Pv": function (t, e) {
      t.exports = "_next/static/images/ban-tinh-04-190b4eb0ad7221e3f1e0e00285e230f0.png";
    },
    "48fX": function (t, e, a) {
      var n = a("qhzo");
      t.exports = function (t, e) {
        if ("function" !== typeof e && null !== e)
          throw new TypeError("Super expression must either be null or a function");
        (t.prototype = Object.create(e && e.prototype, {
          constructor: {
            value: t,
            writable: !0,
            configurable: !0,
          },
        })),
          e && n(t, e);
      };
    },
    "5dWS": function (t, e) {
      t.exports = "_next/static/images/ban-tinh-08-b84ed01a79999015008b849f5626dbb1.png";
    },
    "5fIB": function (t, e, a) {
      var n = a("7eYB");
      t.exports = function (t) {
        if (Array.isArray(t)) return n(t);
      };
    },
    "7Hat": function (t, e) {
      t.exports = "_next/static/images/nut-0c85e61483dad7e4173c34f3e4fb1896.png";
    },
    "7eYB": function (t, e) {
      t.exports = function (t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var a = 0, n = new Array(e); a < e; a++) n[a] = t[a];
        return n;
      };
    },
    "8Kt/": function (t, e, a) {
      "use strict";
      a("oI91");
      (e.__esModule = !0), (e.defaultHead = i), (e.default = void 0);
      var n,
        r = (function (t) {
          if (t && t.__esModule) return t;
          if (null === t || ("object" !== typeof t && "function" !== typeof t))
            return {
              default: t,
            };
          var e = l();
          if (e && e.has(t)) return e.get(t);
          var a = {},
            n = Object.defineProperty && Object.getOwnPropertyDescriptor;
          for (var r in t)
            if (Object.prototype.hasOwnProperty.call(t, r)) {
              var s = n ? Object.getOwnPropertyDescriptor(t, r) : null;
              s && (s.get || s.set) ? Object.defineProperty(a, r, s) : (a[r] = t[r]);
            }
          (a.default = t), e && e.set(t, a);
          return a;
        })(a("q1tI")),
        s =
          (n = a("Xuae")) && n.__esModule
            ? n
            : {
                default: n,
              },
        o = a("lwAK"),
        c = a("FYa8"),
        u = a("/0+H");

      function l() {
        if ("function" !== typeof WeakMap) return null;
        var t = new WeakMap();
        return (
          (l = function () {
            return t;
          }),
          t
        );
      }

      function i() {
        var t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
          e = [
            r.default.createElement("meta", {
              charSet: "utf-8",
            }),
          ];
        return (
          t ||
            e.push(
              r.default.createElement("meta", {
                name: "viewport",
                content: "width=device-width",
              })
            ),
          e
        );
      }

      function h(t, e) {
        return "string" === typeof e || "number" === typeof e
          ? t
          : e.type === r.default.Fragment
          ? t.concat(
              r.default.Children.toArray(e.props.children).reduce(function (t, e) {
                return "string" === typeof e || "number" === typeof e ? t : t.concat(e);
              }, [])
            )
          : t.concat(e);
      }
      var d = ["name", "httpEquiv", "charSet", "itemProp"];

      function b(t, e) {
        return t
          .reduce(function (t, e) {
            var a = r.default.Children.toArray(e.props.children);
            return t.concat(a);
          }, [])
          .reduce(h, [])
          .reverse()
          .concat(i(e.inAmpMode))
          .filter(
            (function () {
              var t = new Set(),
                e = new Set(),
                a = new Set(),
                n = {};
              return function (r) {
                var s = !0;
                if (r.key && "number" !== typeof r.key && r.key.indexOf("$") > 0) {
                  var o = r.key.slice(r.key.indexOf("$") + 1);
                  t.has(o) ? (s = !1) : t.add(o);
                }
                switch (r.type) {
                  case "title":
                  case "base":
                    e.has(r.type) ? (s = !1) : e.add(r.type);
                    break;
                  case "meta":
                    for (var c = 0, u = d.length; c < u; c++) {
                      var l = d[c];
                      if (r.props.hasOwnProperty(l))
                        if ("charSet" === l) a.has(l) ? (s = !1) : a.add(l);
                        else {
                          var i = r.props[l],
                            h = n[l] || new Set();
                          h.has(i) ? (s = !1) : (h.add(i), (n[l] = h));
                        }
                    }
                }
                return s;
              };
            })()
          )
          .reverse()
          .map(function (t, e) {
            var a = t.key || e;
            return r.default.cloneElement(t, {
              key: a,
            });
          });
      }

      function f(t) {
        var e = t.children,
          a = (0, r.useContext)(o.AmpStateContext),
          n = (0, r.useContext)(c.HeadManagerContext);
        return r.default.createElement(
          s.default,
          {
            reduceComponentsToState: b,
            headManager: n,
            inAmpMode: (0, u.isInAmpMode)(a),
          },
          e
        );
      }
      f.rewind = function () {};
      var m = f;
      e.default = m;
    },
    "979+": function (t, e) {
      t.exports = "_next/static/images/ban-tinh-03-e5db5a51e3f1e5e3cf04a65c3c60b8a3.png";
    },
    "9FLd": function (t, e) {
      t.exports = "_next/static/images/ban-tinh-08-c56eb44a3367a56f8b8aa88d877bf423.png";
    },
    Blxu: function (t, e, a) {
      t.exports = {
        beat_loading: "Loading_beat_loading__2e_cR",
      };
    },
    "C+bE": function (t, e) {
      function a(e) {
        return (
          "function" === typeof Symbol && "symbol" === typeof Symbol.iterator
            ? (t.exports = a =
                function (t) {
                  return typeof t;
                })
            : (t.exports = a =
                function (t) {
                  return t && "function" === typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype
                    ? "symbol"
                    : typeof t;
                }),
          a(e)
        );
      }
      t.exports = a;
    },
    E8JP: function (t, e) {
      t.exports = "_next/static/images/hattim-46205aab4e3a1c0988e9592d5b021049.png";
    },
    ENAv: function (t, e) {
      t.exports = "_next/static/images/ban-tinh-06-0104f5322ccfb8a22a85248a4611ca3f.png";
    },
    F34O: function (t, e) {
      t.exports = "_next/static/images/ban-tinh-04-37f1cef5906ff3efaddac0a6faa264a5.png";
    },
    FYa8: function (t, e, a) {
      "use strict";
      var n;
      (e.__esModule = !0), (e.HeadManagerContext = void 0);
      var r = (
        (n = a("q1tI")) && n.__esModule
          ? n
          : {
              default: n,
            }
      ).default.createContext({});
      e.HeadManagerContext = r;
    },
    GKMV: function (t, e) {
      t.exports = "_next/static/images/bigbrainlogo-ce94c7d57b27b72175611455713097eb.png";
    },
    GwoK: function (t, e) {
      t.exports = "_next/static/images/reload-a84519e4b99faca91ec43d87a4cc0264.png";
    },
    H5Qs: function (t, e) {
      t.exports = "_next/static/images/hatdo-460ce64b6d7f96c4a8eae37be2843eae.png";
    },
    "HuV/": function (t, e) {
      t.exports = "_next/static/images/thanh-so-38e2ef12f91f214dc151bb075b98658b.png";
    },
    JtSB: function (t, e) {
      t.exports = "_next/static/images/ban-tinh-07-9d1320f0a0027a7d3304c8c474c7898c.png";
    },
    KX0h: function (t, e) {
      t.exports = "_next/static/images/ban-tinh-06-d7bfee19b08f397b33cc3885d74fc595.png";
    },
    KckH: function (t, e, a) {
      var n = a("7eYB");
      t.exports = function (t, e) {
        if (t) {
          if ("string" === typeof t) return n(t, e);
          var a = Object.prototype.toString.call(t).slice(8, -1);
          return (
            "Object" === a && t.constructor && (a = t.constructor.name),
            "Map" === a || "Set" === a
              ? Array.from(t)
              : "Arguments" === a || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(a)
              ? n(t, e)
              : void 0
          );
        }
      };
    },
    KyhC: function (t, e) {
      t.exports = "_next/static/images/logoMonsterBox-dc029590b7af02125dde029720118cf2.png";
    },
    LDyf: function (t, e, a) {
      "use strict";

      function n(t) {
        for (var a in t) e.hasOwnProperty(a) || (e[a] = t[a]);
      }
      Object.defineProperty(e, "__esModule", {
        value: !0,
      }),
        n(a("PZj6")),
        n(a("Rw4t")),
        n(a("z+Cf"));
    },
    MAsN: function (t, e) {
      t.exports =
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFgAAAA0CAYAAAAOo/8VAAAACXBIWXMAAAsSAAALEgHS3X78AAAQRklEQVR4nMWcbawdR3nH/8/snnPuufa9tm9oUyFCSiOkKiYJNIZUN2rsEBkp4kP50FQKolSiUZSqfGmhSBVCQqrUqmq+9ANKFVoJAnJUAU2rULcqIqEErPLmpAmu2lACBDtRIDH29X09e3aeamZndp6ZnT3nxi/JSON9Pevd3/7Pf56ZZ84lZsbrVX76rpdWFIqbFegtAH7VVAZfa26HNL2Nwctm3d8ju3VudtodGmzqiWYnzmvmpwCcq1k/qaCee+szb/zh6/aA5jleS8Av3PLyUSK6HQpvZ/ANAN7U3AVQKIWCiu6HONSJrgzRZlsH2HbpqznEbCvbpTmdTzDjaWb9GEAnr//va14z6FcU8Iu3vHKIFN6tiX9bEa2WZYmRGkAphVIVUIowUKUFbGta/K1pAdqt70wrsGZU9RS11na5o6sWtBZL7UGbpTYvgU9p8BPMeOzG/7n2C1cMwJUA/LNbfnFdTfqjStF7R+XwmqGBWgxRFAooHEiFANVvt3fkliyWnAD2wGuxXTfVgN+pJ9isd6ziY8gOMDPqAHtNM/8LA4+849m3XHbYlw3wi7e8cu9AlR8cD0a3LpRDFKUDKuvAwfT7KAHui3ZLCXUqQOoErADcnlvDKntzuo1NvYOteqcLWZttbbftkvm0ZnyOiP7+5v/7tctiI5cE+Pl3vbQyxugjw7L8vcXBwjWDQQmUDuQQYd1D9SD74ErIqYJ1AtvUKsCUYNvqzjNWMtE1NuotrNdbqPTUKph1A9y8CA/egTb1YYL6xDufu+6SQF8U4J+804AdfmSxXPjw4nC0rAYKGDmYo0S1RQJYIW8VswonqpXqNRAnrk7FPgFYKtrAW59u4Xy9YUFrHVRdx2r251vQv/njt14U6FcN+Myhl+/aX+y9fzwYvlkNFbDgoJZJVRnQUsVK1JyC0aNe6b+pcg3kbafsFLCrFqDWVsEG9LnpOqYWrO5ArrUOLwD4y4qn9x95/uDZKwL4zKGXrytRfHalXLp1MCqBcQ9YD1Jul8Iaijkqlo1crnHL+W5iCdhxdZKBXHnIDcyprnF+uoEL082gWgG4aQzNPpjzT2viDx1+/vqvXFbAP73553ftVQt/d6BcWrZgx8JfU8ApWJVsF4lylYgiZik4VW7Od1OYRs1bGc+uhJJ1A3VTT3C2Om+B14lNtNDdS2Hgbw+fvv4PLwvg53/jpQf2F0v3LZVj0B6KVTsQyyLZV/QA71Ny6sU59eaUK4HqGGJrGxtuWx6rgifXDrSB+/PqHHbqqmMVtWaxbW3jVEXT295z5qaZltEL+AdvP7MypPLzB4q9d+4dLkItUgNPQk0VvBtFS2UXiUXIeDjnv3Wi2BSw9N4qA3ki9lcAT7m1AK/kStf4xfQCNuvt5AXEKnZRyFoNfcfRF2/87qsC/L83nV4Zovz6gXLp4NJwjGKsQtgl4Q4y+4rMS8h5shJLlYCWPbg0xs1ZQZ1RbpWBvN6FrHUAWAvb+Fl1Ftu6ahtE3UKG8+YW+JpWfMd7eiCr3M4BF19fVKODe8oF0AJ1Q6wi8dJc45YCHSR1JKqPROYthyLGHvTYUdFzn4VrO5L9zTslKGoq2QqslPswpIE7CrdsjtlzzF77YV5Gja/+29X/dWhXgH9w4wvHSioO7i+XQKPmgtmwipKveZH4q9yf2oiH5KuHu+AgjAXsUQJ1lr+n95OGhaW7trh/D1Q2APYxSGFfsQeFCtD9KaHJaI4ZyKz1I4/+8smVmYCfveHMx8B89/5iCWTGYIrmBrJjBynotMHq62ykCveQFzJVqjZVafotQrIPPcLwFiaexwP06nTEMaASe2gcAY3eQrRJbyo0PdoL+NkbXrgOmj++pBbthTHs6V5RpqbH1Yxz0xeSwl4Qqk2B9o1j5F54+n/KMphxLFHoXrUI5R8o+gx1P868+uWrTn4sC7jW9UMEWl5UC81DXUzp6/bOeykqgZ7rfOymSz2vpNfbZdmjxjNPjJTN/PFHrvp2axUW8DPX/+Qoa6wuqFHztuy4d098nPawuOcYMqFW35iCjwZ8V3c7iRByn8ndR+6e0nv3ZYaIwmWaDyzQKPN83Dmv4YtlpdVHI8Ck6Y/NaWN/oSK+3tyH6QOY6+LKWomBGt+93RbbkyTUqucAn7ctK4V1dumn8LAxbKPQEoMIZPoiWQAH8x998cC3rIrVk7/+3IpmfacihZJKl/9iNItm3LRzk7nt3MOnHYK0E+DXd0S31tftDOQqUXba6ciBn1MDW59+8ikotAzM0SZk6yrcnytha2CZmH7HrJd2hYGCi+ZkIvFBcm+XYmgqgSgbmzrjoakk5GfTsYn0KWYN6kwTdcsu8yzw4trsSAawLIAH6NTCD0nYWL/h8+4C7wPwoJHsEbOvRCGShwQmm60FmXXNxkYCvLqn5c6B5QRs7fxPJ41bOuDTN4qWfhvqXSg7V3UAbPJ1NoeXAeXXrQAldMgXg7DeKlvfaRVca762oPC18GkVEw8asKyaGyhqCnDnRQWpb2sBtcgot+/FpCrOpYbSLnMu25EOCNWN/WiZjW4zGhwlTn3CVNu3gVjpHdWjtVdT/2HpPw+VzLxqHcDBJW4GQXSpQUrZfeaTxAw1FV0ZzLGAUiivEMsUbM4e0mtJyOmQpQSXGWDvy3LYZ9RxQjQGK+ExJjwNQBHEmKpei7ZLEw6UFqp5OcQt4HrKFgSxezKtQO47bSHPU2waXdSJanPqTV+YtLlcqig3JpxTcqbaUbSpHHrUUUI0TfmzG8pMXwRH26HRaybDNNBLc0FSwJSn9j8ircwMD9DAQVXKxTJoZG6YVJSPf6VyvdfWu1DuLAWnKu5Tcuq9OQWbIcrawdUc5eOaYchOlrmt23pi97UQe1QP90I8aGMRa5qxvF1PoZWB3YRlesLAiJyDNyq2UndEWsileFjpsyncHOCcj8tGDgJuquQZ6fqOmt22rh3MnXbQPAxHhuFHNwYc1iuuIwXrRLlaqlccM3uNRXwfGqsTNbEXsjcKhelEo7SgDFj2XRKwJhTmgoosZBtd5Dx3liXkIpCcp+cspy+7nCpZx5BbmBNuQHPIUoSxXZGDE9AvTDciu/AKlw2jTiyjcQb1XMnA05p51dzQxnQLy+Ue2FFlA3lLo1g0gBRYaxsjm2lPsF8VE8o1wyDKh3C+MUuHC3ej3t36emoVuaUA3HqqAVlp6O1EuXEaSMyRcGkkrrHFOy3Q2lkHpw1kYisMrH1ga/WHRsGPEeM+84Dr2MJYLaCkhpSNhzcBHgNFQba/YZ2FGiVrYihWzWA1E1RNsz03zSTnRt1SuLO8eIaSIwA6WINUnJxwEq075Zp963qrtQeZcW5tpIl5o//HvgjwcXPr5eHRPV/62tan1xhYhq6xXm9iSe1p7MCANK3uJoMXlZ0BaZ+V2KmZGg8yIbIDbWzaRBqkXNzcF+/OG1Kcp+BcJ0QnMys9zIptm8K191vZwAlYrIUHaxuardUbbpqVgKsD3LiBDPPeQHjcasfseOyN3z+miO42cYOxgDcU+zAqBjDjEwaicjCLcQFVhm3L0KVZQrqF4n3mP1HU7e2l4Vkf4JyCBdw48Jd+2DQ8xnN5O44Igop1gKPTbY1X6nOY2GlWwaOlcju+3Xoz1n6/um0fwqAdfaLW+u7mqw+cxRquov1moAIFK2gD19oFoxgq8NB0QDxoA9kE1QGsFrkr89JMQxhyWbAZA8Ic9fZYRTwnOB5w0a7ltoBNrLvTNGichZvMRUsUfK5et9Nh27kQMroAi5lAMVzXYfuUv/02q/yVX3n6AQLuMwo2oIZUYqU0kJVQrKsFQQ0JalBYwF6tigRYv+5zA4SQNESj6Haguqfv0orY/dOOdslRrKQnZePcnSbezXUa5OzKGG6whg29Za1BAm+tg53n6tykQZtxPl0XfNO9k9vPRoCPX/3kSsnFj4iw7GEO1QAHin1ZyORtY0igUlnolEBu03kuz0UQKm69AUHNPYRZbMT9fQG3MnbQgGXEYIMfI4RXqZqdEjf1Ns7X65GF+M/llN7ahjsGot/9g+nhdp5xNC/i+NVPHVUa/y5hmnHQfeWSzdMpoVQLUgWftd5cStghvR3bg0xrUZcr+c5NomKI32r4UavKWcCUrbV1xgmSwL/T+MlzNDdw9YWOar3fMscdlM63Anj4nvrI+6PHScc2j//SUyaz/FcSsk1hq70YFwuNKkUj1mnczEWLxkZsZpoUqPTW4NQMaROxmjMG0fxbN7Gm2dDTRqkdT0Y8SBP3tjJjDALiml6384dlt1cnMa/shNi4GuHzYJy6Rx95W0cvuZk9X37DyWOKcTeIImsY0Qj7ir3WMjrRgmjUIF5CC1U10YRXOyJr6CqZtcgo1LGKe20CKVzEys1EGhOurCVUpkGTY8PtebEPtzEvh7ltBDqlS77N++5cwKY8unLymJkjoVQceplOyFiNYNL7Ur3NOWgnY0hbkDNhYhX7u0i25WCa+AlX2JQRRNjWwiLkOK37LUbUwJk5wVt6C+t6MwuWMzbAIjyr3Tmz4M4EbMo/H/juA2Dcpzw8AdOANnMGTCa6SGwCSOCKCAIde0hhx4QlXm73BcAQoLsqjgdhAthtbOhtaK7zYFnnfVpYinuOmXDnAjblH/d/514F/DVAyzHoBpT5bZuBvFeNrVenqm1ny2QaNjllaWaY5rEmgNGxhjiEC77cjIht6q122FFzHG2kjZ92aQmpbqNofz9E9LAu+MOz4O4KsClf3P/tQ6TxGSIcVC0456diupGJNEzqv1G17WIkcW9qD2lDNwMyiyCtE//m1Wy6umagZod3Wr9Mo4y8N4dvgNzX3DGtQeFP75keeXAuuN0C9uULy9/6C2L8GUm7gAcdRwqlKjHEwIZ5AxpYG2kbM4qbNZpBWN5eB3Ci4oqnrlbY4cpC9UkdH10gAdp6tN8nXoAE6+ieKFTxwQ9Vt+36BzGv+kcwJpEH5r8h0Kpq1ZnEvUg818yApQKKCoxM/tp2XAqbyS7k/MOelFFqExOb0Wx+WmuAeLBphOG7zmk04ZUf2YDfp1lcpy2nSdGf71a1slz07+SO7T1xlBifJMZqaPwk2KSDITRLwhfkvuxvlZ1yzbhsZBPtkUTRaSQBCddbiVAvx5YRwWns4FNa8f3zvLavXPIvPT+/+M2jxPxJo+go7o0iCbTA/a2jYw2zhtPSzl2mwcvGwwGoX9fyGIeIIClGsZ+7FLDtU10qYF8eGn/jEAF/Qoz3wkwdyoB2G90kcu9gRIjV5F22XWZ0wUKo2QOFtAjx+bQQ0b+C8E8XYwV95Yr82v6zoyfuJeB9YNwK8LK0DXQXWUWH25IOLFcC0HjiXT4u7gUAOkWKHiKiL72axmu35Yr/vYjPDJ+4i5nfTeDfAuhge6AnPKO4fYtKUKgMy7p/sGNWMb7KxMeJ6HFF6qtXAmr0PFcasCwPDh9fKbS6g8HvAPNhgN7s/yjH/I5GsIPdFjuzg/gEgZ4G4cnXAmhaXtc/KePLpwdfO2qWxLjd7mP7F1FsyqUtDPcy+AKInkmvQUT/4VZ/zIQfMfH3LrWBuuQC4P8BzgGmxXaLEKgAAAAASUVORK5CYII=";
    },
    "OL/y": function (t, e) {
      t.exports = "_next/static/images/nut-098c8133ea43fe39dda0ed0aad5c3ee7.png";
    },
    PZj6: function (t, e, a) {
      "use strict";
      Object.defineProperty(e, "__esModule", {
        value: !0,
      });
      var n = {
        loading: !0,
        color: "#000000",
        css: "",
      };

      function r(t) {
        return Object.assign({}, n, {
          size: t,
        });
      }

      function s(t, e) {
        return Object.assign({}, n, {
          height: t,
          width: e,
        });
      }
      (e.sizeDefaults = r),
        (e.sizeMarginDefaults = function (t) {
          return Object.assign({}, r(t), {
            margin: 2,
          });
        }),
        (e.heightWidthDefaults = s),
        (e.heightWidthRadiusDefaults = function (t, e, a) {
          return (
            void 0 === a && (a = 2),
            Object.assign({}, s(t, e), {
              radius: a,
              margin: 2,
            })
          );
        });
    },
    Qetd: function (t, e, a) {
      "use strict";
      var n = Object.assign.bind(Object);
      (t.exports = n), (t.exports.default = t.exports);
    },
    RNiq: function (t, e, a) {
      "use strict";
      a.r(e),
        a.d(e, "default", function () {
          return Ht;
        });
      var n = a("nKUr"),
        r = a("q1tI"),
        s = a.n(r),
        o = a("8Kt/"),
        c = a.n(o),
        u = a("ohBo"),
        l = a.n(u),
        i = a("YRqr"),
        h = a("+b/q"),
        d = a.n(h),
        b = a("GKMV"),
        f = a.n(b),
        m = a("9FLd"),
        g = a.n(m),
        j = a("JtSB"),
        p = a.n(j),
        x = a("KX0h"),
        O = a.n(x),
        y = a("TkXg"),
        v = a.n(y),
        M = a("F34O"),
        R = a.n(M),
        k = a("979+"),
        w = a.n(k),
        T = a("ynQq"),
        C = a.n(T),
        L = a("TyHY"),
        A = a.n(L),
        S = a("5dWS"),
        _ = a.n(S),
        E = a("2GlY"),
        N = a.n(E),
        H = a("ENAv"),
        I = a.n(H),
        F = a("2dTo"),
        z = a.n(F),
        q = a("40Pv"),
        B = a.n(q),
        P = a("RyYW"),
        Z = a.n(P),
        D = a("tkeB"),
        W = a.n(D),
        K = a("HuV/"),
        J = a.n(K),
        Y = a("hf3v"),
        V = a.n(Y),
        U = a("GwoK"),
        G = a.n(U),
        X = a("lKqx"),
        Q = a.n(X);

      function $(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var a = 0, n = new Array(e); a < e; a++) n[a] = t[a];
        return n;
      }

      function tt(t, e) {
        return (
          (function (t) {
            if (Array.isArray(t)) return t;
          })(t) ||
          (function (t, e) {
            if ("undefined" !== typeof Symbol && Symbol.iterator in Object(t)) {
              var a = [],
                n = !0,
                r = !1,
                s = void 0;
              try {
                for (
                  var o, c = t[Symbol.iterator]();
                  !(n = (o = c.next()).done) && (a.push(o.value), !e || a.length !== e);
                  n = !0
                );
              } catch (u) {
                (r = !0), (s = u);
              } finally {
                try {
                  n || null == c.return || c.return();
                } finally {
                  if (r) throw s;
                }
              }
              return a;
            }
          })(t, e) ||
          (function (t, e) {
            if (t) {
              if ("string" === typeof t) return $(t, e);
              var a = Object.prototype.toString.call(t).slice(8, -1);
              return (
                "Object" === a && t.constructor && (a = t.constructor.name),
                "Map" === a || "Set" === a
                  ? Array.from(t)
                  : "Arguments" === a || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(a)
                  ? $(t, e)
                  : void 0
              );
            }
          })(t, e) ||
          (function () {
            throw new TypeError(
              "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
            );
          })()
        );
      }

      function et() {
        return (et =
          Object.assign ||
          function (t) {
            for (var e = 1; e < arguments.length; e++) {
              var a = arguments[e];
              for (var n in a) Object.prototype.hasOwnProperty.call(a, n) && (t[n] = a[n]);
            }
            return t;
          }).apply(this, arguments);
      }
      var at = function (t, e) {
          void 0 === e && (e = {});
          var n,
            o = e,
            c = o.volume,
            u = void 0 === c ? 1 : c,
            l = o.playbackRate,
            i = void 0 === l ? 1 : l,
            h = o.soundEnabled,
            d = void 0 === h || h,
            b = o.interrupt,
            f = void 0 !== b && b,
            m = o.onload,
            g = (function (t, e) {
              if (null == t) return {};
              var a,
                n,
                r = {},
                s = Object.keys(t);
              for (n = 0; n < s.length; n++) (a = s[n]), e.indexOf(a) >= 0 || (r[a] = t[a]);
              return r;
            })(o, ["volume", "playbackRate", "soundEnabled", "interrupt", "onload"]),
            j = s.a.useRef(null),
            p = s.a.useRef(!1),
            x = s.a.useState(!1),
            O = x[0],
            y = x[1],
            v = s.a.useState(null),
            M = v[0],
            R = v[1],
            k = s.a.useState(null),
            w = k[0],
            T = k[1],
            C = function () {
              "function" === typeof m && m.call(this), p.current && R(1e3 * this.duration());
            };
          (n = function () {
            return (
              a
                .e(8)
                .then(a.t.bind(null, "HlzF", 7))
                .then(function (e) {
                  if (!p.current) {
                    (j.current = e.Howl), (p.current = !0);
                    var a = new j.current(
                      et(
                        {
                          src: Array.isArray(t) ? t : [t],
                          volume: u,
                          rate: i,
                          onload: C,
                        },
                        g
                      )
                    );
                    T(a);
                  }
                }),
              function () {
                p.current = !1;
              }
            );
          }),
            Object(r.useEffect)(n, []),
            s.a.useEffect(
              function () {
                j.current &&
                  w &&
                  T(
                    new j.current(
                      et(
                        {
                          src: Array.isArray(t) ? t : [t],
                          volume: u,
                          onload: C,
                        },
                        g
                      )
                    )
                  );
              },
              [JSON.stringify(t)]
            ),
            s.a.useEffect(
              function () {
                w && (w.volume(u), w.rate(i));
              },
              [u, i]
            );
          var L = s.a.useCallback(
              function (t) {
                "undefined" === typeof t && (t = {}),
                  w &&
                    (d || t.forceSoundEnabled) &&
                    (f && w.stop(),
                    t.playbackRate && w.rate(t.playbackRate),
                    w.play(t.id),
                    p.current &&
                      w.once("end", function () {
                        w.playing() || y(!1);
                      }),
                    p.current && y(!0));
              },
              [w, d, f]
            ),
            A = s.a.useCallback(
              function (t) {
                w && (w.stop(t), p.current && y(!1));
              },
              [w]
            ),
            S = s.a.useCallback(
              function (t) {
                w && (w.pause(t), p.current && y(!1));
              },
              [w]
            );
          return [
            L,
            {
              sound: w,
              stop: A,
              pause: S,
              isPlaying: O,
              duration: M,
            },
          ];
        },
        nt = (a("KyhC"), a("twyN")),
        rt = a.n(nt),
        st = a("hQ3+"),
        ot = a.n(st),
        ct = a("H5Qs"),
        ut = a.n(ct),
        lt = function (t) {
          var e = t.reLoading,
            a = t.nutTron,
            s = t.hatMau,
            o = t.column,
            c = (t.result, t.setResult),
            u = t.hatMauRe,
            l = "".concat("https://bantinh.bigbrain.com.vn", "/gay-tinh.mp3"),
            i = tt(at(l), 1)[0],
            h = Object(r.useState)(!1),
            d = h[0],
            b = h[1],
            f = Object(r.useState)(!1),
            m = f[0],
            g = f[1],
            j = Object(r.useState)(!1),
            p = j[0],
            x = j[1],
            O = Object(r.useState)(!1),
            y = O[0],
            v = O[1],
            M = Object(r.useState)(!1),
            R = M[0],
            k = M[1],
            w = Object(r.useRef)(null),
            T = Object(r.useRef)(null),
            C = Object(r.useRef)(null),
            L = Object(r.useRef)(null);
          Object(r.useEffect)(
            function () {
              g(!1), x(!1), v(!1), k(!1), b(!1);
            },
            [e]
          );
          return Object(n.jsxs)("div", {
            className: "hanghat "
              .concat(o, " ")
              .concat(m ? "result-1" : "", " ")
              .concat(p ? "result-2" : "", " ")
              .concat(y ? "result-3" : "", " ")
              .concat(R ? "result-4" : "", " ")
              .concat(d ? "result-5" : ""),
            children: [
              Object(n.jsx)("div", {
                className: "hat-five bg-img",
                onClick: function () {
                  i(),
                    d || m || p || y || R || (b(!0), c(5)),
                    d || !m || p || y || R || (b(!0), c(6)),
                    d && !0 === m && (b(!1), c(1)),
                    !d || m || p || y || R || (b(!1), c(0)),
                    d || !p || y || (b(!0), c(7)),
                    d && p && !y && (b(!1), c(2)),
                    !d && p && y && !R && (b(!0), c(8)),
                    d && p && y && !R && (b(!1), c(3)),
                    !d && p && y && R && (b(!0), c(9)),
                    d && p && y && R && (b(!1), c(4));
                },
                style: {
                  backgroundImage: "url(".concat(rt.a, ")"),
                  backgroundSize: "92px",
                },
              }),
              a &&
                Object(n.jsx)("div", {
                  className: "nut-tron",
                  style: {
                    backgroundImage: "url(".concat(a, ")")
                  },
                }),
              Object(n.jsxs)("div", {
                className: "hat-one",
                children: [
                  Object(n.jsx)("div", {
                    ref: w,
                    className: "one-1 bg-img",
                    onClick: function () {
                      i(),
                        m || d || R || (g(!0), c(1)),
                        m || !d || R || (g(!0), c(6)),
                        m && d && !p && !R && (g(!1), c(5)),
                        !m || d || p || R || (g(!1), c(0)),
                        m && R && !d && (k(!1), g(!1), x(!1), v(!1), c(0)),
                        m && R && d && (k(!1), g(!1), x(!1), v(!1), c(5)),
                        m && !R && y && d && (k(!1), g(!1), x(!1), v(!1), c(5)),
                        m && !R && y && !d && (k(!1), g(!1), x(!1), v(!1), c(0)),
                        !m || d || !p || y || R || (g(!1), x(!1), v(!1), k(!1), c(0)),
                        m && d && p && !y && !R && (g(!1), x(!1), v(!1), k(!1), c(5));
                    },
                    style: {
                      backgroundImage: "url(".concat(s, ")"),
                      backgroundSize: "92px",
                    },
                  }),
                  Object(n.jsx)("div", {
                    ref: T,
                    className: "one-2 bg-img",
                    onClick: function () {
                      i(),
                        p || !m || y || R || d || (x(!0), c(2)),
                        !p || !m || y || R || d || (x(!1), c(1)),
                        p || !d || !m || y || R || (x(!0), c(7)),
                        p && d && m && !y && !R && (x(!1), c(6)),
                        p && !d && m && y && R && (v(!1), k(!1), g(!0), x(!1), c(6)),
                        p && !d && m && y && !R && (v(!1), k(!1), g(!0), x(!1), c(1)),
                        p && d && m && y && R && (v(!1), k(!1), g(!0), x(!1), c(6)),
                        p && d && m && y && !R && (v(!1), k(!1), g(!0), x(!1), c(6)),
                        p || d || m || y || R || (v(!1), k(!1), g(!0), x(!0), c(2)),
                        p || !d || m || y || R || (v(!1), k(!1), g(!0), x(!0), c(7));
                    },
                    style: {
                      backgroundImage: "url(".concat(u, ")"),
                      backgroundSize: "92px",
                    },
                  }),
                  Object(n.jsx)("div", {
                    ref: C,
                    className: "one-3 bg-img",
                    onClick: function () {
                      i(),
                        y || !p || d || R || (v(!0), c(3)),
                        y && p && !d && !R && (v(!1), c(2)),
                        !y && p && d && !R && (v(!0), c(8)),
                        y && p && d && !R && (v(!1), c(7)),
                        y || !d || m || p || R || (v(!0), k(!1), g(!0), x(!0), c(8)),
                        y || d || m || p || R || (g(!0), x(!0), v(!0), k(!1), c(3)),
                        y || !d || !m || p || R || (v(!0), k(!1), g(!0), x(!0), c(8)),
                        y || d || !m || p || R || (g(!0), x(!0), v(!0), k(!1), c(3)),
                        y && !d && m && p && R && (g(!0), x(!0), v(!1), k(!1), c(2)),
                        y && d && m && p && R && (g(!0), x(!0), v(!1), k(!1), c(7));
                    },
                    style: {
                      backgroundImage: "url(".concat(u, ")"),
                      backgroundSize: "92px",
                    },
                  }),
                  Object(n.jsx)("div", {
                    ref: L,
                    className: "one-4 bg-img",
                    onClick: function () {
                      i(),
                        R || !y || d || (k(!0), c(4)),
                        R && y && !d && (k(!1), c(3)),
                        !R && y && d && (k(!0), c(9)),
                        R && y && d && (k(!1), c(8)),
                        R || m || d || (k(!0), g(!0), x(!0), v(!0), c(4)),
                        R || m || !d || (k(!0), g(!0), x(!0), v(!0), c(9)),
                        R || !m || d || (k(!0), g(!0), x(!0), v(!0), c(4)),
                        !R && m && d && (k(!0), g(!0), x(!0), v(!0), c(9));
                    },
                    style: {
                      backgroundImage: "url(".concat(u, ")"),
                      backgroundSize: "92px",
                    },
                  }),
                ],
              }),
            ],
          });
        },
        it = a("k60j"),
        ht = a.n(it),
        dt = a("MAsN"),
        bt = a.n(dt),
        ft = a("jpb/"),
        mt = a.n(ft),
        gt = a("dPD4"),
        jt = a.n(gt),
        pt = a("E8JP"),
        xt = a.n(pt),
        Ot = a("7Hat"),
        yt = a.n(Ot),
        vt = a("OL/y"),
        Mt = a.n(vt),
        Rt = function (t) {
          var e = t.reLoading,
            a = t.sorobanNum,
            o = t.decimalNum,
            c = t.decimalActivate,
            u = t.setDecimal04,
            l = t.setDecimal03,
            i = t.setDecimal02,
            h = t.setDecimal01,
            d = t.setResult08,
            b = t.setResult07,
            f = t.setResult06,
            m = t.setResult05,
            g = t.setResult04,
            j = t.setResult03,
            p = t.setResult02,
            x = t.setResult01,
            O = t.decimal04,
            y = t.decimal03,
            v = t.decimal02,
            M = t.decimal01,
            R = t.result08,
            k = t.result07,
            w = t.result06,
            T = t.result05,
            C = t.result04,
            L = t.result03,
            A = t.result02,
            S = t.result01,
            _ = t.sorobanColor,
            E = t.hatMau,
            N = Object(r.useState)(ot.a),
            H = N[0],
            I = N[1];
          return (
            Object(r.useEffect)(
              function () {
                switch (E) {
                  case "yellow":
                    I(ot.a);
                    break;
                  case "blue":
                    I(ht.a);
                    break;
                  case "pink":
                    I(bt.a);
                    break;
                  case "luc":
                    I(mt.a);
                    break;
                  case "tia":
                    I(jt.a);
                    break;
                  case "tim":
                    I(xt.a);
                }
              },
              [E]
            ),
            Object(n.jsxs)(s.a.Fragment, {
              children: [
                8 === a &&
                  Object(n.jsxs)(n.Fragment, {
                    children: [
                      c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            4 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: g,
                                    result: C,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-08",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-07",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: u,
                                    result: O,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            3 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: m,
                                    result: T,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-08",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: g,
                                    result: C,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-07",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            2 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: f,
                                    result: w,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-08",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: m,
                                    result: T,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-07",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: g,
                                    result: C,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            1 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: b,
                                    result: k,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-08",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: f,
                                    result: w,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-07",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: m,
                                    result: T,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: g,
                                    result: C,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                          ],
                        }),
                      !c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: d,
                              result: R,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-08",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: b,
                              result: k,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-07",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: f,
                              result: w,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-06",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: m,
                              result: T,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-05",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: g,
                              result: C,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-04",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: j,
                              result: L,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-03",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: p,
                              result: A,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-02",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: x,
                              result: S,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-01",
                            }),
                          ],
                        }),
                    ],
                  }),
                7 === a &&
                  Object(n.jsxs)(n.Fragment, {
                    children: [
                      c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            4 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-07",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: u,
                                    result: O,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            3 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: g,
                                    result: C,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-07",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            2 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: m,
                                    result: T,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-07",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: g,
                                    result: C,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            1 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: f,
                                    result: w,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-07",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: m,
                                    result: T,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: g,
                                    result: C,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                          ],
                        }),
                      !c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: b,
                              result: k,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-07",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: f,
                              result: w,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-06",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: m,
                              result: T,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-05",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: g,
                              result: C,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-04",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: j,
                              result: L,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-03",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: p,
                              result: A,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-02",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: x,
                              result: S,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-01",
                            }),
                          ],
                        }),
                    ],
                  }),
                6 === a &&
                  Object(n.jsxs)(n.Fragment, {
                    children: [
                      c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            4 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: u,
                                    result: O,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            3 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            2 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: g,
                                    result: C,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            1 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: m,
                                    result: T,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-06",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: g,
                                    result: C,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                          ],
                        }),
                      !c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: f,
                              result: w,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-06",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: m,
                              result: T,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-05",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: g,
                              result: C,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-04",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: j,
                              result: L,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-03",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: p,
                              result: A,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-02",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: x,
                              result: S,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-01",
                            }),
                          ],
                        }),
                    ],
                  }),
                5 === a &&
                  Object(n.jsxs)(n.Fragment, {
                    children: [
                      c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            4 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: u,
                                    result: O,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            3 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            2 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            1 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: g,
                                    result: C,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-05",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                          ],
                        }),
                      !c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: m,
                              result: T,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-05",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: g,
                              result: C,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-04",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: j,
                              result: L,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-03",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: p,
                              result: A,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-02",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: x,
                              result: S,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-01",
                            }),
                          ],
                        }),
                    ],
                  }),
                4 === a &&
                  Object(n.jsxs)(n.Fragment, {
                    children: [
                      c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            4 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: u,
                                    result: O,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            3 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            2 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            1 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: j,
                                    result: L,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-04",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                          ],
                        }),
                      !c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: g,
                              result: C,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-04",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: j,
                              result: L,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-03",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: p,
                              result: A,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-02",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: x,
                              result: S,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-01",
                            }),
                          ],
                        }),
                    ],
                  }),
                3 === a &&
                  Object(n.jsxs)(n.Fragment, {
                    children: [
                      c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            3 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: l,
                                    result: y,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            2 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            1 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: p,
                                    result: A,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-03",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                          ],
                        }),
                      !c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: j,
                              result: L,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-03",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: p,
                              result: A,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-02",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: x,
                              result: S,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-01",
                            }),
                          ],
                        }),
                    ],
                  }),
                2 === a &&
                  Object(n.jsxs)(n.Fragment, {
                    children: [
                      c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            2 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: i,
                                    result: v,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                            1 === o &&
                              Object(n.jsxs)(n.Fragment, {
                                children: [
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: x,
                                    result: S,
                                    nutTron: "yellow" === _ ? yt.a : Mt.a,
                                    hatMau: ut.a,
                                    hatMauRe: H,
                                    column: "hanghat-02",
                                  }),
                                  Object(n.jsx)(lt, {
                                    reLoading: e,
                                    setResult: h,
                                    result: M,
                                    nutTron: null,
                                    hatMau: H,
                                    hatMauRe: H,
                                    column: "hanghat-01",
                                  }),
                                ],
                              }),
                          ],
                        }),
                      !c &&
                        Object(n.jsxs)(n.Fragment, {
                          children: [
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: p,
                              result: A,
                              nutTron: null,
                              hatMau: H,
                              hatMauRe: H,
                              column: "hanghat-02",
                            }),
                            Object(n.jsx)(lt, {
                              reLoading: e,
                              setResult: x,
                              result: S,
                              nutTron: "yellow" === _ ? yt.a : Mt.a,
                              hatMau: ut.a,
                              hatMauRe: H,
                              column: "hanghat-01",
                            }),
                          ],
                        }),
                    ],
                  }),
              ],
            })
          );
        };
      var kt = a("/aNb"),
        wt = a.n(kt),
        Tt = a("qKvR"),
        Ct = a("Blxu"),
        Lt = a.n(Ct);

      function At() {
        var t,
          e,
          a =
            ((t = ["\n        display: block;\n        margin: 0 auto;\n        border-color: red;\n    "]),
            e || (e = t.slice(0)),
            Object.freeze(
              Object.defineProperties(t, {
                raw: {
                  value: Object.freeze(e),
                },
              })
            ));
        return (
          (At = function () {
            return a;
          }),
          a
        );
      }
      var St = function () {
          var t = Object(Tt.css)(At());
          return Object(n.jsx)(n.Fragment, {
            children: Object(n.jsx)("div", {
              className: Lt.a.beat_loading,
              children: Object(n.jsx)(wt.a, {
                css: t,
                size: 15,
                margin: 2,
                color: "#dddddd",
                loading: !0,
              }),
            }),
          });
        },
        _t = a("jkOO"),
        Et = a.n(_t);

      function Nt() {
        return {
          width: window.innerWidth,
          height: window.innerHeight,
        };
      }

      function Ht() {
        var t = (function () {
            var t = Object(r.useState)(Nt()),
              e = t[0],
              a = t[1];
            Object(r.useEffect)(function () {
              return (
                window.addEventListener("resize", n),
                function () {
                  window.removeEventListener("resize", n);
                }
              );
            });
            var n = Object(r.useCallback)(function () {
              a(Nt());
            });
            return e;
          })().width,
          e = Object(i.b)().getTheme,
          a = Object(r.useState)(0),
          s = a[0],
          o = a[1],
          u = Object(r.useState)(0),
          h = u[0],
          b = u[1],
          m = Object(r.useState)(0),
          j = m[0],
          x = m[1],
          y = Object(r.useState)(0),
          M = y[0],
          k = y[1],
          T = Object(r.useState)(0),
          L = T[0],
          S = T[1],
          E = Object(r.useState)(0),
          H = E[0],
          F = E[1],
          q = Object(r.useState)(0),
          P = q[0],
          D = q[1],
          K = Object(r.useState)(0),
          Y = K[0],
          U = K[1],
          X = Object(r.useState)(0),
          $ = X[0],
          tt = X[1],
          et = Object(r.useState)(0),
          at = et[0],
          nt = et[1],
          rt = Object(r.useState)(0),
          st = rt[0],
          ot = rt[1],
          ct = Object(r.useState)(0),
          ut = ct[0],
          lt = ct[1],
          it = Object(r.useState)("yellow"),
          ht = it[0],
          dt = it[1],
          bt = Object(r.useState)("yellow"),
          ft = bt[0],
          mt = bt[1],
          gt = Object(r.useState)(!1),
          jt = gt[0],
          pt = gt[1],
          xt = Object(r.useState)(!1),
          Ot = xt[0],
          yt = xt[1],
          vt = Object(r.useState)(!1),
          Mt = vt[0],
          kt = vt[1],
          wt = Object(r.useState)(8),
          Tt = wt[0],
          Ct = wt[1],
          Lt = Object(r.useState)(4),
          At = Lt[0],
          _t = Lt[1],
          Ht = Object(r.useState)(!1),
          It = Ht[0],
          Ft = Ht[1],
          zt = Object(r.useState)(!0),
          qt = zt[0],
          Bt = zt[1],
          Pt = Object(r.useState)(8),
          Zt = Pt[0],
          Dt = Pt[1],
          Wt = Object(r.useState)(4),
          Kt = Wt[0],
          Jt = Wt[1],
          Yt = Object(r.useState)("yellow"),
          Vt = Yt[0],
          Ut = Yt[1],
          Gt = Object(r.useState)("yellow"),
          Xt = Gt[0],
          Qt = Gt[1],
          $t = Object(r.useState)(!1),
          te = $t[0],
          ee = $t[1],
          ae = Object(r.useState)(0),
          ne = ae[0],
          re = ae[1],
          se = Object(r.useRef)(null),
          oe = Object(r.useRef)(null);
        Object(r.useEffect)(
          function () {
            var t =
                s / 1e4 +
                h / 1e3 +
                j / 100 +
                M / 10 +
                1e7 * ut +
                1e6 * st +
                1e5 * at +
                1e4 * $ +
                1e3 * Y +
                100 * P +
                10 * H +
                1 * L,
              e = Math.round(1e5 * t) / 1e5;
            re(e);
          },
          [s, h, j, M, L, H, P, Y, $, at, st, ut]
        );
        var ce = Object(r.useMemo)(
          function () {
            return ne;
          },
          [ne]
        );
        Object(r.useEffect)(function () {
          oe.current && se.current && (se.current.style.height = oe.current.offsetHeight + "px");
        }, []);
        var ue = Object(r.useState)(!1),
          le = ue[0],
          ie = ue[1];
        Object(r.useEffect)(
          function () {
            switch (t <= 768) {
              case !0:
                ie(!0);
                break;
              case !1:
                ie(!1);
            }
          },
          [t]
        ),
          Object(r.useEffect)(
            function () {
              le &&
                (o(0),
                b(0),
                x(0),
                k(0),
                S(0),
                F(0),
                D(0),
                U(0),
                tt(0),
                nt(0),
                ot(0),
                lt(0),
                Ct(3),
                _t(3),
                pt(!!Ot),
                ee(!0),
                setTimeout(function () {
                  ee(!1);
                }, 450));
            },
            [le]
          );
        return Object(n.jsxs)("div", {
          ref: oe,
          className: d.a.container,
          children: [
            Object(n.jsxs)(c.a, {
              children: [
                Object(n.jsx)("title", {
                  children: "B-smart Soroban - Soroban Applications",
                }),
                Object(n.jsx)("link", {
                  rel: "icon",
                  href: "favicon.ico",
                }),
              ],
            }),
            Object(n.jsx)("div", {
              className: d.a.menu,
              children: Object(n.jsx)("button", {
                onClick: function () {
                  return Ft(!0);
                },
                children: Object(n.jsx)("img", {
                  src: Q.a,
                }),
              }),
            }),
            Object(n.jsx)("div", {
              onClick: function (t) {
                t.preventDefault(), (location.href = "#");
              },
              className: d.a.logo_soroban,
              children: Object(n.jsx)("img", {
                src: Et.a,
              }),
            }),
            Object(n.jsxs)("aside", {
              ref: se,
              className: "".concat(d.a.left_sidebar, " ").concat(It ? d.a.active : ""),
              children: [
                Object(n.jsx)("div", {
                  className: d.a.close_menu,
                  children: Object(n.jsx)("button", {
                    onClick: function () {
                      return Ft(!1);
                    },
                    children: Object(n.jsx)("img", {
                      src: V.a,
                    }),
                  }),
                }),
                Object(n.jsx)("div", {
                  className: d.a.box,
                  children: Object(n.jsxs)("div", {
                    className: d.a.filter,
                    children: [
                      Object(n.jsxs)("div", {
                        className: d.a.select_box,
                        children: [
                          Object(n.jsxs)("div", {
                            className: d.a.decimal,
                            children: [
                              Object(n.jsx)("div", {
                                className: d.a.name,
                                children: Object(n.jsx)("span", {
                                  children: "Ch\u1ecdn m\xe0u n\u1ec1n:",
                                }),
                              }),
                              Object(n.jsxs)("div", {
                                className: d.a.soroban_color,
                                children: [
                                  Object(n.jsx)("button", {
                                    className: ""
                                      .concat(d.a.set_color, " ")
                                      .concat(d.a.light_theme, " ")
                                      .concat(!0 === qt && d.a.active),
                                    onClick: function () {
                                      Bt(!0), e(!0);
                                    },
                                  }),
                                  Object(n.jsx)("button", {
                                    className: ""
                                      .concat(d.a.set_color, " ")
                                      .concat(d.a.dark_theme, " ")
                                      .concat(!1 === qt && d.a.active),
                                    onClick: function () {
                                      Bt(!1), e(!1);
                                    },
                                  }),
                                ],
                              }),
                            ],
                          }),
                          Object(n.jsxs)("div", {
                            className: d.a.decimal,
                            children: [
                              Object(n.jsx)("div", {
                                className: d.a.name,
                                children: Object(n.jsx)("span", {
                                  children: "Ch\u1ecdn m\xe0u b\xe0n t\xednh:",
                                }),
                              }),
                              Object(n.jsxs)("div", {
                                className: d.a.soroban_color,
                                children: [
                                  Object(n.jsx)("button", {
                                    className: ""
                                      .concat(d.a.set_color, " ")
                                      .concat(d.a.yellow, " ")
                                      .concat("yellow" === Vt && d.a.active),
                                    onClick: function () {
                                      Ut("yellow"), dt("yellow");
                                    },
                                  }),
                                  Object(n.jsx)("button", {
                                    className: ""
                                      .concat(d.a.set_color, " ")
                                      .concat(d.a.blue, " ")
                                      .concat("blue" === Vt && d.a.active),
                                    onClick: function () {
                                      Ut("blue"), dt("blue");
                                    },
                                  }),
                                ],
                              }),
                            ],
                          }),
                          Object(n.jsxs)("div", {
                            className: d.a.decimal,
                            children: [
                              Object(n.jsx)("div", {
                                className: d.a.name,
                                children: Object(n.jsx)("span", {
                                  children: "Ch\u1ecdn m\xe0u h\u1ea1t t\xednh:",
                                }),
                              }),
                              Object(n.jsxs)("div", {
                                className: d.a.soroban_color,
                                children: [
                                  Object(n.jsx)("button", {
                                    className: ""
                                      .concat(d.a.set_color, " ")
                                      .concat(d.a.yellow, " ")
                                      .concat("yellow" === Xt && d.a.active),
                                    onClick: function () {
                                      Qt("yellow"), mt("yellow");
                                    },
                                  }),
                                  Object(n.jsx)("button", {
                                    className: ""
                                      .concat(d.a.set_color, " ")
                                      .concat(d.a.blue, " ")
                                      .concat("blue" === Xt && d.a.active),
                                    onClick: function () {
                                      Qt("blue"), mt("blue");
                                    },
                                  }),
                                  Object(n.jsx)("button", {
                                    className: ""
                                      .concat(d.a.set_color, " ")
                                      .concat(d.a.pink, " ")
                                      .concat("pink" === Xt && d.a.active),
                                    onClick: function () {
                                      Qt("pink"), mt("pink");
                                    },
                                  }),
                                  Object(n.jsx)("button", {
                                    className: ""
                                      .concat(d.a.set_color, " ")
                                      .concat(d.a.luc, " ")
                                      .concat("luc" === Xt && d.a.active),
                                    onClick: function () {
                                      Qt("luc"), mt("luc");
                                    },
                                  }),
                                  Object(n.jsx)("button", {
                                    className: ""
                                      .concat(d.a.set_color, " ")
                                      .concat(d.a.tia, " ")
                                      .concat("tia" === Xt && d.a.active),
                                    onClick: function () {
                                      Qt("tia"), mt("tia");
                                    },
                                  }),
                                  Object(n.jsx)("button", {
                                    className: ""
                                      .concat(d.a.set_color, " ")
                                      .concat(d.a.tim, " ")
                                      .concat("tim" === Xt && d.a.active),
                                    onClick: function () {
                                      Qt("tim"), mt("tim");
                                    },
                                  }),
                                ],
                              }),
                            ],
                          }),
                          Object(n.jsxs)("div", {
                            className: d.a.decimal,
                            children: [
                              Object(n.jsx)("div", {
                                className: d.a.name,
                                children: Object(n.jsx)("span", {
                                  children: "H\u1ec7 s\u1ed1 t\u1ef1 nhi\xean:",
                                }),
                              }),
                              Object(n.jsxs)("div", {
                                className: d.a.soroban_num,
                                children: [
                                  Object(n.jsx)("button", {
                                    className: "".concat(d.a.set_num, " ").concat(8 === Zt && d.a.active),
                                    onClick: function () {
                                      return Dt(8);
                                    },
                                    children: "8",
                                  }),
                                  Object(n.jsx)("button", {
                                    className: "".concat(d.a.set_num, " ").concat(7 === Zt && d.a.active),
                                    onClick: function () {
                                      return Dt(7);
                                    },
                                    children: "7",
                                  }),
                                  Object(n.jsx)("button", {
                                    className: "".concat(d.a.set_num, " ").concat(6 === Zt && d.a.active),
                                    onClick: function () {
                                      return Dt(6);
                                    },
                                    children: "6",
                                  }),
                                  Object(n.jsx)("button", {
                                    className: "".concat(d.a.set_num, " ").concat(5 === Zt && d.a.active),
                                    onClick: function () {
                                      return Dt(5);
                                    },
                                    children: "5",
                                  }),
                                  Object(n.jsx)("button", {
                                    className: "".concat(d.a.set_num, " ").concat(4 === Zt && d.a.active),
                                    onClick: function () {
                                      Dt(4), Jt(4);
                                    },
                                    children: "4",
                                  }),
                                  Object(n.jsx)("button", {
                                    className: "".concat(d.a.set_num, " ").concat(3 === Zt && d.a.active),
                                    onClick: function () {
                                      Dt(3), Jt(3);
                                    },
                                    children: "3",
                                  }),
                                  Object(n.jsx)("button", {
                                    className: "".concat(d.a.set_num, " ").concat(2 === Zt && d.a.active),
                                    onClick: function () {
                                      Dt(2), Jt(2);
                                    },
                                    children: "2",
                                  }),
                                ],
                              }),
                            ],
                          }),
                          Object(n.jsxs)("div", {
                            className: "".concat(d.a.decimal, " ").concat(d.a.decimal_name),
                            children: [
                              Object(n.jsx)("div", {
                                className: d.a.name,
                                children: Object(n.jsx)("span", {
                                  children: "Th\xeam h\u1ec7 s\u1ed1 th\u1eadp ph\xe2n?",
                                }),
                              }),
                              Object(n.jsx)("div", {
                                className: "".concat(d.a.value, " ").concat(d.a.switch),
                                children: Object(n.jsx)(l.a, {
                                  onChange: function () {
                                    yt(!Ot);
                                  },
                                  checked: Ot,
                                  onColor: "#fccb90",
                                  onHandleColor: "#ffae5e",
                                  offColor: "#fccb90",
                                  offHandleColor: "#ffae5e",
                                  handleDiameter: 30,
                                  boxShadow: "0px 1px 5px rgba(233, 156, 81, 0.6)",
                                  activeBoxShadow: "0px 0px 1px 10px rgba(233, 156, 81, 0.2)",
                                  uncheckedIcon: Object(n.jsx)("div", {
                                    style: {
                                      display: "flex",
                                      justifyContent: "center",
                                      alignItems: "center",
                                      height: "100%",
                                      fontSize: 10,
                                      color: "#9c3f00",
                                      fontWeight: 600,
                                    },
                                    children: "OFF",
                                  }),
                                  checkedIcon: Object(n.jsx)("div", {
                                    style: {
                                      display: "flex",
                                      justifyContent: "center",
                                      alignItems: "center",
                                      height: "100%",
                                      fontSize: 10,
                                      color: "#9c3f00",
                                      fontWeight: 600,
                                    },
                                    children: "ON",
                                  }),
                                  height: 28,
                                  width: 56,
                                }),
                              }),
                              Ot &&
                                Object(n.jsxs)("div", {
                                  className: d.a.decimal_num,
                                  children: [
                                    Zt >= 4 &&
                                      Object(n.jsx)("button", {
                                        className: "".concat(d.a.set_num, " ").concat(4 === Kt && d.a.active),
                                        onClick: function () {
                                          return Jt(4);
                                        },
                                        children: "4",
                                      }),
                                    Zt >= 3 &&
                                      Object(n.jsx)("button", {
                                        className: "".concat(d.a.set_num, " ").concat(3 === Kt && d.a.active),
                                        onClick: function () {
                                          return Jt(3);
                                        },
                                        children: "3",
                                      }),
                                    Zt >= 2 &&
                                      Object(n.jsxs)(n.Fragment, {
                                        children: [
                                          Object(n.jsx)("button", {
                                            className: "".concat(d.a.set_num, " ").concat(2 === Kt && d.a.active),
                                            onClick: function () {
                                              return Jt(2);
                                            },
                                            children: "2",
                                          }),
                                          Object(n.jsx)("button", {
                                            className: "".concat(d.a.set_num, " ").concat(1 === Kt && d.a.active),
                                            onClick: function () {
                                              return Jt(1);
                                            },
                                            children: "1",
                                          }),
                                        ],
                                      }),
                                  ],
                                }),
                            ],
                          }),
                          Object(n.jsxs)("div", {
                            className: "".concat(d.a.decimal, " ").concat(d.a.decimal_name),
                            children: [
                              Object(n.jsx)("div", {
                                className: d.a.name,
                                children: Object(n.jsx)("span", {
                                  children: "Hi\u1ec7n k\u1ebft qu\u1ea3:",
                                }),
                              }),
                              Object(n.jsx)("div", {
                                className: "".concat(d.a.value, " ").concat(d.a.switch),
                                children: Object(n.jsx)(l.a, {
                                  onChange: function () {
                                    kt(!Mt);
                                  },
                                  checked: Mt,
                                  onColor: "#fccb90",
                                  onHandleColor: "#ffae5e",
                                  offColor: "#fccb90",
                                  offHandleColor: "#ffae5e",
                                  handleDiameter: 30,
                                  boxShadow: "0px 1px 5px rgba(233, 156, 81, 0.6)",
                                  activeBoxShadow: "0px 0px 1px 10px rgba(233, 156, 81, 0.2)",
                                  uncheckedIcon: Object(n.jsx)("div", {
                                    style: {
                                      display: "flex",
                                      justifyContent: "center",
                                      alignItems: "center",
                                      height: "100%",
                                      fontSize: 10,
                                      color: "#9c3f00",
                                      fontWeight: 600,
                                    },
                                    children: "OFF",
                                  }),
                                  checkedIcon: Object(n.jsx)("div", {
                                    style: {
                                      display: "flex",
                                      justifyContent: "center",
                                      alignItems: "center",
                                      height: "100%",
                                      fontSize: 10,
                                      color: "#9c3f00",
                                      fontWeight: 600,
                                    },
                                    children: "ON",
                                  }),
                                  height: 28,
                                  width: 56,
                                }),
                              }),
                            ],
                          }),
                        ],
                      }),
                      Object(n.jsx)("div", {
                        className: d.a.cta_reload,
                        children: Object(n.jsx)("button", {
                          className: d.a.reload_soroban,
                          onClick: function () {
                            return (
                              o(0),
                              b(0),
                              x(0),
                              k(0),
                              S(0),
                              F(0),
                              D(0),
                              U(0),
                              tt(0),
                              nt(0),
                              ot(0),
                              lt(0),
                              Ct(Zt),
                              _t(Kt),
                              dt(Vt),
                              mt(Xt),
                              e(qt),
                              pt(!!Ot),
                              ee(!0),
                              void setTimeout(function () {
                                ee(!1);
                              }, 450)
                            );
                          },
                          children: "T\u1ea3i l\u1ea1i b\xe0n t\xednh",
                        }),
                      }),
                    ],
                  }),
                }),
              ],
            }),
            Object(n.jsxs)("main", {
              className: d.a.main,
              children: [
                Object(n.jsx)("div", {
                  className: d.a.reload_icon,
                  children: Object(n.jsx)("button", {
                    onClick: function (t) {
                      t.preventDefault(),
                        o(0),
                        b(0),
                        x(0),
                        k(0),
                        S(0),
                        F(0),
                        D(0),
                        U(0),
                        tt(0),
                        nt(0),
                        ot(0),
                        lt(0),
                        pt(!!Ot),
                        ee(!0),
                        setTimeout(function () {
                          ee(!1);
                        }, 450);
                    },
                    children: Object(n.jsx)("img", {
                      src: G.a,
                    }),
                  }),
                }),
                Object(n.jsxs)("div", {
                  className: d.a.grid,
                  children: [
                    Mt &&
                      Object(n.jsx)("div", {
                        className: d.a.code,
                        style:
                          "blue" === ht
                            ? {
                                backgroundImage: "url(".concat(A.a, ")"),
                              }
                            : {
                                backgroundImage: "url(".concat(J.a, ")"),
                              },
                        children: Object(n.jsx)("span", {
                          children: ce,
                        }),
                      }),
                    Object(n.jsxs)("div", {
                      className: "bantinh "
                        .concat(8 === Tt ? "ban-tinh-08" : "", " ")
                        .concat(7 === Tt ? "ban-tinh-07" : "", " ")
                        .concat(6 === Tt ? "ban-tinh-06" : "", " ")
                        .concat(5 === Tt ? "ban-tinh-05" : "", " ")
                        .concat(4 === Tt ? "ban-tinh-04" : "", " ")
                        .concat(3 === Tt ? "ban-tinh-03" : "", " ")
                        .concat(2 === Tt ? "ban-tinh-02" : ""),
                      style:
                        "yellow" === ht
                          ? {
                              backgroundImage: "url("
                                .concat(8 === Tt ? g.a : "")
                                .concat(7 === Tt ? p.a : "")
                                .concat(6 === Tt ? O.a : "")
                                .concat(5 === Tt ? v.a : "")
                                .concat(4 === Tt ? R.a : "")
                                .concat(3 === Tt ? w.a : "")
                                .concat(2 === Tt ? C.a : "", ")"),
                            }
                          : {
                              backgroundImage: "url("
                                .concat(8 === Tt ? _.a : "")
                                .concat(7 === Tt ? N.a : "")
                                .concat(6 === Tt ? I.a : "")
                                .concat(5 === Tt ? z.a : "")
                                .concat(4 === Tt ? B.a : "")
                                .concat(3 === Tt ? Z.a : "")
                                .concat(2 === Tt ? W.a : "", ")"),
                            },
                      children: [
                        te && Object(n.jsx)(St, {}),
                        !te &&
                          Object(n.jsx)(Rt, {
                            sorobanNum: Tt,
                            decimalNum: At,
                            decimalActivate: jt,
                            setResult08: lt,
                            setResult07: ot,
                            setResult06: nt,
                            setResult05: tt,
                            setResult04: U,
                            setResult03: D,
                            setResult02: F,
                            setResult01: S,
                            setDecimal01: k,
                            setDecimal02: x,
                            setDecimal03: b,
                            setDecimal04: o,
                            reLoading: te,
                            result08: ut,
                            result07: st,
                            result06: at,
                            result05: $,
                            result04: Y,
                            result03: P,
                            result02: H,
                            result01: L,
                            decimal01: M,
                            decimal02: j,
                            decimal03: h,
                            decimal04: s,
                            sorobanColor: ht,
                            hatMau: ft,
                          }),
                      ],
                    }),
                  ],
                }),
              ],
            }),
            Object(n.jsxs)("footer", {
              className: "".concat(d.a.footer, " footer"),
              children: ["Copyright by B-smart", " "],
            }),
          ],
        });
      }
    },
    Rw4t: function (t, e, a) {
      "use strict";
      var n;
      Object.defineProperty(e, "__esModule", {
        value: !0,
      }),
        (function (t) {
          (t.maroon = "#800000"),
            (t.red = "#FF0000"),
            (t.orange = "#FFA500"),
            (t.yellow = "#FFFF00"),
            (t.olive = "#808000"),
            (t.green = "#008000"),
            (t.purple = "#800080"),
            (t.fuchsia = "#FF00FF"),
            (t.lime = "#00FF00"),
            (t.teal = "#008080"),
            (t.aqua = "#00FFFF"),
            (t.blue = "#0000FF"),
            (t.navy = "#000080"),
            (t.black = "#000000"),
            (t.gray = "#808080"),
            (t.silver = "#C0C0C0"),
            (t.white = "#FFFFFF");
        })(n || (n = {})),
        (e.calculateRgba = function (t, e) {
          if ((Object.keys(n).includes(t) && (t = n[t]), "#" === t[0] && (t = t.slice(1)), 3 === t.length)) {
            var a = "";
            t.split("").forEach(function (t) {
              (a += t), (a += t);
            }),
              (t = a);
          }
          return (
            "rgba(" +
            t
              .match(/.{2}/g)
              .map(function (t) {
                return parseInt(t, 16);
              })
              .join(", ") +
            ", " +
            e +
            ")"
          );
        });
    },
    RyYW: function (t, e) {
      t.exports = "_next/static/images/ban-tinh-03-b3c803fff253953f4b7794486aff76dc.png";
    },
    SksO: function (t, e) {
      function a(e, n) {
        return (
          (t.exports = a =
            Object.setPrototypeOf ||
            function (t, e) {
              return (t.__proto__ = e), t;
            }),
          (t.exports.default = t.exports),
          (t.exports.__esModule = !0),
          a(e, n)
        );
      }
      (t.exports = a), (t.exports.default = t.exports), (t.exports.__esModule = !0);
    },
    T0f4: function (t, e) {
      function a(e) {
        return (
          (t.exports = a =
            Object.setPrototypeOf
              ? Object.getPrototypeOf
              : function (t) {
                  return t.__proto__ || Object.getPrototypeOf(t);
                }),
          a(e)
        );
      }
      t.exports = a;
    },
    TkXg: function (t, e) {
      t.exports = "_next/static/images/ban-tinh-05-6ca90b609c65d984aa658061d4cea795.png";
    },
    TyHY: function (t, e) {
      t.exports = "_next/static/images/thanh-so-25d4db5b85e32839104fe7ebfe68a8fe.png";
    },
    VbXa: function (t, e, a) {
      var n = a("SksO");
      (t.exports = function (t, e) {
        (t.prototype = Object.create(e.prototype)), (t.prototype.constructor = t), n(t, e);
      }),
        (t.exports.default = t.exports),
        (t.exports.__esModule = !0);
    },
    Xuae: function (t, e, a) {
      "use strict";
      var n = a("mPvQ"),
        r = a("/GRZ"),
        s = a("i2R6"),
        o = (a("qXWd"), a("48fX")),
        c = a("tCBg"),
        u = a("T0f4");

      function l(t) {
        var e = (function () {
          if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
          if (Reflect.construct.sham) return !1;
          if ("function" === typeof Proxy) return !0;
          try {
            return Date.prototype.toString.call(Reflect.construct(Date, [], function () {})), !0;
          } catch (t) {
            return !1;
          }
        })();
        return function () {
          var a,
            n = u(t);
          if (e) {
            var r = u(this).constructor;
            a = Reflect.construct(n, arguments, r);
          } else a = n.apply(this, arguments);
          return c(this, a);
        };
      }
      (e.__esModule = !0), (e.default = void 0);
      var i = a("q1tI"),
        h = (function (t) {
          o(a, t);
          var e = l(a);

          function a(t) {
            var s;
            return (
              r(this, a),
              ((s = e.call(this, t))._hasHeadManager = void 0),
              (s.emitChange = function () {
                s._hasHeadManager &&
                  s.props.headManager.updateHead(
                    s.props.reduceComponentsToState(n(s.props.headManager.mountedInstances), s.props)
                  );
              }),
              (s._hasHeadManager = s.props.headManager && s.props.headManager.mountedInstances),
              s
            );
          }
          return (
            s(a, [
              {
                key: "componentDidMount",
                value: function () {
                  this._hasHeadManager && this.props.headManager.mountedInstances.add(this), this.emitChange();
                },
              },
              {
                key: "componentDidUpdate",
                value: function () {
                  this.emitChange();
                },
              },
              {
                key: "componentWillUnmount",
                value: function () {
                  this._hasHeadManager && this.props.headManager.mountedInstances.delete(this), this.emitChange();
                },
              },
              {
                key: "render",
                value: function () {
                  return null;
                },
              },
            ]),
            a
          );
        })(i.Component);
      e.default = h;
    },
    YRqr: function (t, e, a) {
      "use strict";
      a.d(e, "b", function () {
        return o;
      }),
        a.d(e, "a", function () {
          return c;
        });
      var n = a("nKUr"),
        r = a("q1tI"),
        s = Object(r.createContext)();

      function o() {
        return Object(r.useContext)(s);
      }

      function c(t) {
        var e = t.children,
          a = Object(r.useState)(!0),
          o = a[0],
          c = a[1];
        Object(r.useEffect)(
          function () {
            console.log(o),
              o ? document.body.classList.toggle("darkTheme", o) : document.body.classList.remove("darkTheme");
          },
          [o]
        );
        var u = {
          isDark: o,
          getTheme: function (t) {
            c(t);
          },
        };
        return Object(n.jsx)(s.Provider, {
          value: u,
          children: e,
        });
      }
    },
    dPD4: function (t, e) {
      t.exports = "_next/static/images/hattia-9401ebfe1bd6a166ae3203e28b85a3b4.png";
    },
    "hQ3+": function (t, e) {
      t.exports = "_next/static/images/hatvang-cca682259e8459305cb5fd68d9a325b3.png";
    },
    hf3v: function (t, e) {
      t.exports = "_next/static/images/close-88dea02064779fdceb54c03aa971c53b.png";
    },
    i2R6: function (t, e) {
      function a(t, e) {
        for (var a = 0; a < e.length; a++) {
          var n = e[a];
          (n.enumerable = n.enumerable || !1),
            (n.configurable = !0),
            "value" in n && (n.writable = !0),
            Object.defineProperty(t, n.key, n);
        }
      }
      t.exports = function (t, e, n) {
        return e && a(t.prototype, e), n && a(t, n), t;
      };
    },
    jkOO: function (t, e) {
      t.exports = "_next/static/images/logo-9e916377b85ff86174d06e124b7baf13.png";
    },
    "jpb/": function (t, e) {
      t.exports = "_next/static/images/hatluc-6d89bba174955dc1d2c833ea9aa92c57.png";
    },
    k60j: function (t, e) {
      t.exports = "_next/static/images/hatxanhbien-a4ef58ef904ca38f143239f8b12fd29b.png";
    },
    kG2m: function (t, e) {
      t.exports = function () {
        throw new TypeError(
          "Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
        );
      };
    },
    lKqx: function (t, e) {
      t.exports = "_next/static/images/setting-66a0a470d6094aced77d5ee2cccc40af.png";
    },
    lwAK: function (t, e, a) {
      "use strict";
      var n;
      (e.__esModule = !0), (e.AmpStateContext = void 0);
      var r = (
        (n = a("q1tI")) && n.__esModule
          ? n
          : {
              default: n,
            }
      ).default.createContext({});
      e.AmpStateContext = r;
    },
    mPvQ: function (t, e, a) {
      var n = a("5fIB"),
        r = a("rlHP"),
        s = a("KckH"),
        o = a("kG2m");
      t.exports = function (t) {
        return n(t) || r(t) || s(t) || o();
      };
    },
    oI91: function (t, e) {
      t.exports = function (t, e, a) {
        return (
          e in t
            ? Object.defineProperty(t, e, {
                value: a,
                enumerable: !0,
                configurable: !0,
                writable: !0,
              })
            : (t[e] = a),
          t
        );
      };
    },
    ohBo: function (t, e, a) {
      t.exports = a("1Mdp");
    },
    qKvR: function (t, e, a) {
      "use strict";

      function n(t, e) {
        return (n =
          Object.setPrototypeOf ||
          function (t, e) {
            return (t.__proto__ = e), t;
          })(t, e);
      }
      a.r(e),
        a.d(e, "CacheProvider", function () {
          return C;
        }),
        a.d(e, "ThemeContext", function () {
          return T;
        }),
        a.d(e, "withEmotionCache", function () {
          return L;
        }),
        a.d(e, "css", function () {
          return N;
        }),
        a.d(e, "ClassNames", function () {
          return P;
        }),
        a.d(e, "Global", function () {
          return I;
        }),
        a.d(e, "createElement", function () {
          return H;
        }),
        a.d(e, "jsx", function () {
          return H;
        }),
        a.d(e, "keyframes", function () {
          return z;
        });
      var r = a("q1tI");
      var s = (function () {
        function t(t) {
          (this.isSpeedy = void 0 === t.speedy || t.speedy),
            (this.tags = []),
            (this.ctr = 0),
            (this.nonce = t.nonce),
            (this.key = t.key),
            (this.container = t.container),
            (this.before = null);
        }
        var e = t.prototype;
        return (
          (e.insert = function (t) {
            if (this.ctr % (this.isSpeedy ? 65e3 : 1) === 0) {
              var e,
                a = (function (t) {
                  var e = document.createElement("style");
                  return (
                    e.setAttribute("data-emotion", t.key),
                    void 0 !== t.nonce && e.setAttribute("nonce", t.nonce),
                    e.appendChild(document.createTextNode("")),
                    e
                  );
                })(this);
              (e = 0 === this.tags.length ? this.before : this.tags[this.tags.length - 1].nextSibling),
                this.container.insertBefore(a, e),
                this.tags.push(a);
            }
            var n = this.tags[this.tags.length - 1];
            if (this.isSpeedy) {
              var r = (function (t) {
                if (t.sheet) return t.sheet;
                for (var e = 0; e < document.styleSheets.length; e++)
                  if (document.styleSheets[e].ownerNode === t) return document.styleSheets[e];
              })(n);
              try {
                var s = 105 === t.charCodeAt(1) && 64 === t.charCodeAt(0);
                r.insertRule(t, s ? 0 : r.cssRules.length);
              } catch (o) {
                0;
              }
            } else n.appendChild(document.createTextNode(t));
            this.ctr++;
          }),
          (e.flush = function () {
            this.tags.forEach(function (t) {
              return t.parentNode.removeChild(t);
            }),
              (this.tags = []),
              (this.ctr = 0);
          }),
          t
        );
      })();
      var o = function (t) {
        function e(t, e, n) {
          var r = e.trim().split(f);
          e = r;
          var s = r.length,
            o = t.length;
          switch (o) {
            case 0:
            case 1:
              var c = 0;
              for (t = 0 === o ? "" : t[0] + " "; c < s; ++c) e[c] = a(t, e[c], n).trim();
              break;
            default:
              var u = (c = 0);
              for (e = []; c < s; ++c) for (var l = 0; l < o; ++l) e[u++] = a(t[l] + " ", r[c], n).trim();
          }
          return e;
        }

        function a(t, e, a) {
          var n = e.charCodeAt(0);
          switch ((33 > n && (n = (e = e.trim()).charCodeAt(0)), n)) {
            case 38:
              return e.replace(m, "$1" + t.trim());
            case 58:
              return t.trim() + e.replace(m, "$1" + t.trim());
            default:
              if (0 < 1 * a && 0 < e.indexOf("\f"))
                return e.replace(m, (58 === t.charCodeAt(0) ? "" : "$1") + t.trim());
          }
          return t + e;
        }

        function n(t, e, a, s) {
          var o = t + ";",
            c = 2 * e + 3 * a + 4 * s;
          if (944 === c) {
            t = o.indexOf(":", 9) + 1;
            var u = o.substring(t, o.length - 1).trim();
            return (u = o.substring(0, t).trim() + u + ";"), 1 === L || (2 === L && r(u, 1)) ? "-webkit-" + u + u : u;
          }
          if (0 === L || (2 === L && !r(o, 1))) return o;
          switch (c) {
            case 1015:
              return 97 === o.charCodeAt(10) ? "-webkit-" + o + o : o;
            case 951:
              return 116 === o.charCodeAt(3) ? "-webkit-" + o + o : o;
            case 963:
              return 110 === o.charCodeAt(5) ? "-webkit-" + o + o : o;
            case 1009:
              if (100 !== o.charCodeAt(4)) break;
            case 969:
            case 942:
              return "-webkit-" + o + o;
            case 978:
              return "-webkit-" + o + "-moz-" + o + o;
            case 1019:
            case 983:
              return "-webkit-" + o + "-moz-" + o + "-ms-" + o + o;
            case 883:
              if (45 === o.charCodeAt(8)) return "-webkit-" + o + o;
              if (0 < o.indexOf("image-set(", 11)) return o.replace(k, "$1-webkit-$2") + o;
              break;
            case 932:
              if (45 === o.charCodeAt(4))
                switch (o.charCodeAt(5)) {
                  case 103:
                    return (
                      "-webkit-box-" +
                      o.replace("-grow", "") +
                      "-webkit-" +
                      o +
                      "-ms-" +
                      o.replace("grow", "positive") +
                      o
                    );
                  case 115:
                    return "-webkit-" + o + "-ms-" + o.replace("shrink", "negative") + o;
                  case 98:
                    return "-webkit-" + o + "-ms-" + o.replace("basis", "preferred-size") + o;
                }
              return "-webkit-" + o + "-ms-" + o + o;
            case 964:
              return "-webkit-" + o + "-ms-flex-" + o + o;
            case 1023:
              if (99 !== o.charCodeAt(8)) break;
              return (
                "-webkit-box-pack" +
                (u = o.substring(o.indexOf(":", 15)).replace("flex-", "").replace("space-between", "justify")) +
                "-webkit-" +
                o +
                "-ms-flex-pack" +
                u +
                o
              );
            case 1005:
              return d.test(o) ? o.replace(h, ":-webkit-") + o.replace(h, ":-moz-") + o : o;
            case 1e3:
              switch (((e = (u = o.substring(13).trim()).indexOf("-") + 1), u.charCodeAt(0) + u.charCodeAt(e))) {
                case 226:
                  u = o.replace(x, "tb");
                  break;
                case 232:
                  u = o.replace(x, "tb-rl");
                  break;
                case 220:
                  u = o.replace(x, "lr");
                  break;
                default:
                  return o;
              }
              return "-webkit-" + o + "-ms-" + u + o;
            case 1017:
              if (-1 === o.indexOf("sticky", 9)) break;
            case 975:
              switch (
                ((e = (o = t).length - 10),
                (c =
                  (u = (33 === o.charCodeAt(e) ? o.substring(0, e) : o)
                    .substring(t.indexOf(":", 7) + 1)
                    .trim()).charCodeAt(0) +
                  (0 | u.charCodeAt(7))))
              ) {
                case 203:
                  if (111 > u.charCodeAt(8)) break;
                case 115:
                  o = o.replace(u, "-webkit-" + u) + ";" + o;
                  break;
                case 207:
                case 102:
                  o =
                    o.replace(u, "-webkit-" + (102 < c ? "inline-" : "") + "box") +
                    ";" +
                    o.replace(u, "-webkit-" + u) +
                    ";" +
                    o.replace(u, "-ms-" + u + "box") +
                    ";" +
                    o;
              }
              return o + ";";
            case 938:
              if (45 === o.charCodeAt(5))
                switch (o.charCodeAt(6)) {
                  case 105:
                    return (u = o.replace("-items", "")), "-webkit-" + o + "-webkit-box-" + u + "-ms-flex-" + u + o;
                  case 115:
                    return "-webkit-" + o + "-ms-flex-item-" + o.replace(v, "") + o;
                  default:
                    return "-webkit-" + o + "-ms-flex-line-pack" + o.replace("align-content", "").replace(v, "") + o;
                }
              break;
            case 973:
            case 989:
              if (45 !== o.charCodeAt(3) || 122 === o.charCodeAt(4)) break;
            case 931:
            case 953:
              if (!0 === R.test(t))
                return 115 === (u = t.substring(t.indexOf(":") + 1)).charCodeAt(0)
                  ? n(t.replace("stretch", "fill-available"), e, a, s).replace(":fill-available", ":stretch")
                  : o.replace(u, "-webkit-" + u) + o.replace(u, "-moz-" + u.replace("fill-", "")) + o;
              break;
            case 962:
              if (
                ((o = "-webkit-" + o + (102 === o.charCodeAt(5) ? "-ms-" + o : "") + o),
                211 === a + s && 105 === o.charCodeAt(13) && 0 < o.indexOf("transform", 10))
              )
                return o.substring(0, o.indexOf(";", 27) + 1).replace(b, "$1-webkit-$2") + o;
          }
          return o;
        }

        function r(t, e) {
          var a = t.indexOf(1 === e ? ":" : "{"),
            n = t.substring(0, 3 !== e ? a : 10);
          return (a = t.substring(a + 1, t.length - 1)), E(2 !== e ? n : n.replace(M, "$1"), a, e);
        }

        function s(t, e) {
          var a = n(e, e.charCodeAt(0), e.charCodeAt(1), e.charCodeAt(2));
          return a !== e + ";" ? a.replace(y, " or ($1)").substring(4) : "(" + e + ")";
        }

        function o(t, e, a, n, r, s, o, c, l, i) {
          for (var h, d = 0, b = e; d < _; ++d)
            switch ((h = S[d].call(u, t, b, a, n, r, s, o, c, l, i))) {
              case void 0:
              case !1:
              case !0:
              case null:
                break;
              default:
                b = h;
            }
          if (b !== e) return b;
        }

        function c(t) {
          return (
            void 0 !== (t = t.prefix) &&
              ((E = null), t ? ("function" !== typeof t ? (L = 1) : ((L = 2), (E = t))) : (L = 0)),
            c
          );
        }

        function u(t, a) {
          var c = t;
          if ((33 > c.charCodeAt(0) && (c = c.trim()), (c = [c]), 0 < _)) {
            var u = o(-1, a, c, c, T, w, 0, 0, 0, 0);
            void 0 !== u && "string" === typeof u && (a = u);
          }
          var h = (function t(a, c, u, h, d) {
            for (
              var b,
                f,
                m,
                x,
                y,
                v = 0,
                M = 0,
                R = 0,
                k = 0,
                S = 0,
                E = 0,
                H = (m = b = 0),
                I = 0,
                F = 0,
                z = 0,
                q = 0,
                B = u.length,
                P = B - 1,
                Z = "",
                D = "",
                W = "",
                K = "";
              I < B;

            ) {
              if (
                ((f = u.charCodeAt(I)),
                I === P && 0 !== M + k + R + v && (0 !== M && (f = 47 === M ? 10 : 47), (k = R = v = 0), B++, P++),
                0 === M + k + R + v)
              ) {
                if (I === P && (0 < F && (Z = Z.replace(i, "")), 0 < Z.trim().length)) {
                  switch (f) {
                    case 32:
                    case 9:
                    case 59:
                    case 13:
                    case 10:
                      break;
                    default:
                      Z += u.charAt(I);
                  }
                  f = 59;
                }
                switch (f) {
                  case 123:
                    for (b = (Z = Z.trim()).charCodeAt(0), m = 1, q = ++I; I < B; ) {
                      switch ((f = u.charCodeAt(I))) {
                        case 123:
                          m++;
                          break;
                        case 125:
                          m--;
                          break;
                        case 47:
                          switch ((f = u.charCodeAt(I + 1))) {
                            case 42:
                            case 47:
                              t: {
                                for (H = I + 1; H < P; ++H)
                                  switch (u.charCodeAt(H)) {
                                    case 47:
                                      if (42 === f && 42 === u.charCodeAt(H - 1) && I + 2 !== H) {
                                        I = H + 1;
                                        break t;
                                      }
                                      break;
                                    case 10:
                                      if (47 === f) {
                                        I = H + 1;
                                        break t;
                                      }
                                  }
                                I = H;
                              }
                          }
                          break;
                        case 91:
                          f++;
                        case 40:
                          f++;
                        case 34:
                        case 39:
                          for (; I++ < P && u.charCodeAt(I) !== f; );
                      }
                      if (0 === m) break;
                      I++;
                    }
                    switch (
                      ((m = u.substring(q, I)), 0 === b && (b = (Z = Z.replace(l, "").trim()).charCodeAt(0)), b)
                    ) {
                      case 64:
                        switch ((0 < F && (Z = Z.replace(i, "")), (f = Z.charCodeAt(1)))) {
                          case 100:
                          case 109:
                          case 115:
                          case 45:
                            F = c;
                            break;
                          default:
                            F = A;
                        }
                        if (
                          ((q = (m = t(c, F, m, f, d + 1)).length),
                          0 < _ &&
                            ((y = o(3, m, (F = e(A, Z, z)), c, T, w, q, f, d, h)),
                            (Z = F.join("")),
                            void 0 !== y && 0 === (q = (m = y.trim()).length) && ((f = 0), (m = ""))),
                          0 < q)
                        )
                          switch (f) {
                            case 115:
                              Z = Z.replace(O, s);
                            case 100:
                            case 109:
                            case 45:
                              m = Z + "{" + m + "}";
                              break;
                            case 107:
                              (m = (Z = Z.replace(g, "$1 $2")) + "{" + m + "}"),
                                (m = 1 === L || (2 === L && r("@" + m, 3)) ? "@-webkit-" + m + "@" + m : "@" + m);
                              break;
                            default:
                              (m = Z + m), 112 === h && ((D += m), (m = ""));
                          }
                        else m = "";
                        break;
                      default:
                        m = t(c, e(c, Z, z), m, h, d + 1);
                    }
                    (W += m), (m = z = F = H = b = 0), (Z = ""), (f = u.charCodeAt(++I));
                    break;
                  case 125:
                  case 59:
                    if (1 < (q = (Z = (0 < F ? Z.replace(i, "") : Z).trim()).length))
                      switch (
                        (0 === H &&
                          ((b = Z.charCodeAt(0)), 45 === b || (96 < b && 123 > b)) &&
                          (q = (Z = Z.replace(" ", ":")).length),
                        0 < _ &&
                          void 0 !== (y = o(1, Z, c, a, T, w, D.length, h, d, h)) &&
                          0 === (q = (Z = y.trim()).length) &&
                          (Z = "\0\0"),
                        (b = Z.charCodeAt(0)),
                        (f = Z.charCodeAt(1)),
                        b)
                      ) {
                        case 0:
                          break;
                        case 64:
                          if (105 === f || 99 === f) {
                            K += Z + u.charAt(I);
                            break;
                          }
                        default:
                          58 !== Z.charCodeAt(q - 1) && (D += n(Z, b, f, Z.charCodeAt(2)));
                      }
                    (z = F = H = b = 0), (Z = ""), (f = u.charCodeAt(++I));
                }
              }
              switch (f) {
                case 13:
                case 10:
                  47 === M ? (M = 0) : 0 === 1 + b && 107 !== h && 0 < Z.length && ((F = 1), (Z += "\0")),
                    0 < _ * N && o(0, Z, c, a, T, w, D.length, h, d, h),
                    (w = 1),
                    T++;
                  break;
                case 59:
                case 125:
                  if (0 === M + k + R + v) {
                    w++;
                    break;
                  }
                default:
                  switch ((w++, (x = u.charAt(I)), f)) {
                    case 9:
                    case 32:
                      if (0 === k + v + M)
                        switch (S) {
                          case 44:
                          case 58:
                          case 9:
                          case 32:
                            x = "";
                            break;
                          default:
                            32 !== f && (x = " ");
                        }
                      break;
                    case 0:
                      x = "\\0";
                      break;
                    case 12:
                      x = "\\f";
                      break;
                    case 11:
                      x = "\\v";
                      break;
                    case 38:
                      0 === k + M + v && ((F = z = 1), (x = "\f" + x));
                      break;
                    case 108:
                      if (0 === k + M + v + C && 0 < H)
                        switch (I - H) {
                          case 2:
                            112 === S && 58 === u.charCodeAt(I - 3) && (C = S);
                          case 8:
                            111 === E && (C = E);
                        }
                      break;
                    case 58:
                      0 === k + M + v && (H = I);
                      break;
                    case 44:
                      0 === M + R + k + v && ((F = 1), (x += "\r"));
                      break;
                    case 34:
                    case 39:
                      0 === M && (k = k === f ? 0 : 0 === k ? f : k);
                      break;
                    case 91:
                      0 === k + M + R && v++;
                      break;
                    case 93:
                      0 === k + M + R && v--;
                      break;
                    case 41:
                      0 === k + M + v && R--;
                      break;
                    case 40:
                      if (0 === k + M + v) {
                        if (0 === b)
                          switch (2 * S + 3 * E) {
                            case 533:
                              break;
                            default:
                              b = 1;
                          }
                        R++;
                      }
                      break;
                    case 64:
                      0 === M + R + k + v + H + m && (m = 1);
                      break;
                    case 42:
                    case 47:
                      if (!(0 < k + v + R))
                        switch (M) {
                          case 0:
                            switch (2 * f + 3 * u.charCodeAt(I + 1)) {
                              case 235:
                                M = 47;
                                break;
                              case 220:
                                (q = I), (M = 42);
                            }
                            break;
                          case 42:
                            47 === f &&
                              42 === S &&
                              q + 2 !== I &&
                              (33 === u.charCodeAt(q + 2) && (D += u.substring(q, I + 1)), (x = ""), (M = 0));
                        }
                  }
                  0 === M && (Z += x);
              }
              (E = S), (S = f), I++;
            }
            if (0 < (q = D.length)) {
              if (((F = c), 0 < _ && void 0 !== (y = o(2, D, F, a, T, w, q, h, d, h)) && 0 === (D = y).length))
                return K + D + W;
              if (((D = F.join(",") + "{" + D + "}"), 0 !== L * C)) {
                switch ((2 !== L || r(D, 2) || (C = 0), C)) {
                  case 111:
                    D = D.replace(p, ":-moz-$1") + D;
                    break;
                  case 112:
                    D =
                      D.replace(j, "::-webkit-input-$1") +
                      D.replace(j, "::-moz-$1") +
                      D.replace(j, ":-ms-input-$1") +
                      D;
                }
                C = 0;
              }
            }
            return K + D + W;
          })(A, c, a, 0, 0);
          return (
            0 < _ && void 0 !== (u = o(-2, h, c, c, T, w, h.length, 0, 0, 0)) && (h = u), "", (C = 0), (w = T = 1), h
          );
        }
        var l = /^\0+/g,
          i = /[\0\r\f]/g,
          h = /: */g,
          d = /zoo|gra/,
          b = /([,: ])(transform)/g,
          f = /,\r+?/g,
          m = /([\t\r\n ])*\f?&/g,
          g = /@(k\w+)\s*(\S*)\s*/,
          j = /::(place)/g,
          p = /:(read-only)/g,
          x = /[svh]\w+-[tblr]{2}/,
          O = /\(\s*(.*)\s*\)/g,
          y = /([\s\S]*?);/g,
          v = /-self|flex-/g,
          M = /[^]*?(:[rp][el]a[\w-]+)[^]*/,
          R = /stretch|:\s*\w+\-(?:conte|avail)/,
          k = /([^-])(image-set\()/,
          w = 1,
          T = 1,
          C = 0,
          L = 1,
          A = [],
          S = [],
          _ = 0,
          E = null,
          N = 0;
        return (
          (u.use = function t(e) {
            switch (e) {
              case void 0:
              case null:
                _ = S.length = 0;
                break;
              default:
                if ("function" === typeof e) S[_++] = e;
                else if ("object" === typeof e) for (var a = 0, n = e.length; a < n; ++a) t(e[a]);
                else N = 0 | !!e;
            }
            return t;
          }),
          (u.set = c),
          void 0 !== t && c(t),
          u
        );
      };

      function c(t) {
        t && u.current.insert(t + "}");
      }
      var u = {
          current: null,
        },
        l = function (t, e, a, n, r, s, o, l, i, h) {
          switch (t) {
            case 1:
              switch (e.charCodeAt(0)) {
                case 64:
                  return u.current.insert(e + ";"), "";
                case 108:
                  if (98 === e.charCodeAt(2)) return "";
              }
              break;
            case 2:
              if (0 === l) return e + "/*|*/";
              break;
            case 3:
              switch (l) {
                case 102:
                case 112:
                  return u.current.insert(a[0] + e), "";
                default:
                  return e + (0 === h ? "/*|*/" : "");
              }
            case -2:
              e.split("/*|*/}").forEach(c);
          }
        },
        i = function (t) {
          void 0 === t && (t = {});
          var e,
            a = t.key || "css";
          void 0 !== t.prefix &&
            (e = {
              prefix: t.prefix,
            });
          var n = new o(e);
          var r,
            c = {};
          r = t.container || document.head;
          var i,
            h = document.querySelectorAll("style[data-emotion-" + a + "]");
          Array.prototype.forEach.call(h, function (t) {
            t
              .getAttribute("data-emotion-" + a)
              .split(" ")
              .forEach(function (t) {
                c[t] = !0;
              }),
              t.parentNode !== r && r.appendChild(t);
          }),
            n.use(t.stylisPlugins)(l),
            (i = function (t, e, a, r) {
              var s = e.name;
              (u.current = a), n(t, e.styles), r && (d.inserted[s] = !0);
            });
          var d = {
            key: a,
            sheet: new s({
              key: a,
              container: r,
              nonce: t.nonce,
              speedy: t.speedy,
            }),
            nonce: t.nonce,
            inserted: c,
            registered: {},
            insert: i,
          };
          return d;
        };
      a("VbXa");

      function h(t, e, a) {
        var n = "";
        return (
          a.split(" ").forEach(function (a) {
            void 0 !== t[a] ? e.push(t[a]) : (n += a + " ");
          }),
          n
        );
      }
      var d = function (t, e, a) {
        var n = t.key + "-" + e.name;
        if ((!1 === a && void 0 === t.registered[n] && (t.registered[n] = e.styles), void 0 === t.inserted[e.name])) {
          var r = e;
          do {
            t.insert("." + n, r, t.sheet, !0);
            r = r.next;
          } while (void 0 !== r);
        }
      };
      var b = function (t) {
          for (var e, a = 0, n = 0, r = t.length; r >= 4; ++n, r -= 4)
            (e =
              1540483477 *
                (65535 &
                  (e =
                    (255 & t.charCodeAt(n)) |
                    ((255 & t.charCodeAt(++n)) << 8) |
                    ((255 & t.charCodeAt(++n)) << 16) |
                    ((255 & t.charCodeAt(++n)) << 24))) +
              ((59797 * (e >>> 16)) << 16)),
              (a =
                (1540483477 * (65535 & (e ^= e >>> 24)) + ((59797 * (e >>> 16)) << 16)) ^
                (1540483477 * (65535 & a) + ((59797 * (a >>> 16)) << 16)));
          switch (r) {
            case 3:
              a ^= (255 & t.charCodeAt(n + 2)) << 16;
            case 2:
              a ^= (255 & t.charCodeAt(n + 1)) << 8;
            case 1:
              a = 1540483477 * (65535 & (a ^= 255 & t.charCodeAt(n))) + ((59797 * (a >>> 16)) << 16);
          }
          return (
            ((a = 1540483477 * (65535 & (a ^= a >>> 13)) + ((59797 * (a >>> 16)) << 16)) ^ (a >>> 15)) >>>
            0
          ).toString(36);
        },
        f = {
          animationIterationCount: 1,
          borderImageOutset: 1,
          borderImageSlice: 1,
          borderImageWidth: 1,
          boxFlex: 1,
          boxFlexGroup: 1,
          boxOrdinalGroup: 1,
          columnCount: 1,
          columns: 1,
          flex: 1,
          flexGrow: 1,
          flexPositive: 1,
          flexShrink: 1,
          flexNegative: 1,
          flexOrder: 1,
          gridRow: 1,
          gridRowEnd: 1,
          gridRowSpan: 1,
          gridRowStart: 1,
          gridColumn: 1,
          gridColumnEnd: 1,
          gridColumnSpan: 1,
          gridColumnStart: 1,
          msGridRow: 1,
          msGridRowSpan: 1,
          msGridColumn: 1,
          msGridColumnSpan: 1,
          fontWeight: 1,
          lineHeight: 1,
          opacity: 1,
          order: 1,
          orphans: 1,
          tabSize: 1,
          widows: 1,
          zIndex: 1,
          zoom: 1,
          WebkitLineClamp: 1,
          fillOpacity: 1,
          floodOpacity: 1,
          stopOpacity: 1,
          strokeDasharray: 1,
          strokeDashoffset: 1,
          strokeMiterlimit: 1,
          strokeOpacity: 1,
          strokeWidth: 1,
        };
      var m = /[A-Z]|^ms/g,
        g = /_EMO_([^_]+?)_([^]*?)_EMO_/g,
        j = function (t) {
          return 45 === t.charCodeAt(1);
        },
        p = function (t) {
          return null != t && "boolean" !== typeof t;
        },
        x = (function (t) {
          var e = {};
          return function (a) {
            return void 0 === e[a] && (e[a] = t(a)), e[a];
          };
        })(function (t) {
          return j(t) ? t : t.replace(m, "-$&").toLowerCase();
        }),
        O = function (t, e) {
          switch (t) {
            case "animation":
            case "animationName":
              if ("string" === typeof e)
                return e.replace(g, function (t, e, a) {
                  return (
                    (v = {
                      name: e,
                      styles: a,
                      next: v,
                    }),
                    e
                  );
                });
          }
          return 1 === f[t] || j(t) || "number" !== typeof e || 0 === e ? e : e + "px";
        };

      function y(t, e, a, n) {
        if (null == a) return "";
        if (void 0 !== a.__emotion_styles) return a;
        switch (typeof a) {
          case "boolean":
            return "";
          case "object":
            if (1 === a.anim)
              return (
                (v = {
                  name: a.name,
                  styles: a.styles,
                  next: v,
                }),
                a.name
              );
            if (void 0 !== a.styles) {
              var r = a.next;
              if (void 0 !== r)
                for (; void 0 !== r; )
                  (v = {
                    name: r.name,
                    styles: r.styles,
                    next: v,
                  }),
                    (r = r.next);
              return a.styles + ";";
            }
            return (function (t, e, a) {
              var n = "";
              if (Array.isArray(a)) for (var r = 0; r < a.length; r++) n += y(t, e, a[r], !1);
              else
                for (var s in a) {
                  var o = a[s];
                  if ("object" !== typeof o)
                    null != e && void 0 !== e[o]
                      ? (n += s + "{" + e[o] + "}")
                      : p(o) && (n += x(s) + ":" + O(s, o) + ";");
                  else if (!Array.isArray(o) || "string" !== typeof o[0] || (null != e && void 0 !== e[o[0]])) {
                    var c = y(t, e, o, !1);
                    switch (s) {
                      case "animation":
                      case "animationName":
                        n += x(s) + ":" + c + ";";
                        break;
                      default:
                        n += s + "{" + c + "}";
                    }
                  } else for (var u = 0; u < o.length; u++) p(o[u]) && (n += x(s) + ":" + O(s, o[u]) + ";");
                }
              return n;
            })(t, e, a);
          case "function":
            if (void 0 !== t) {
              var s = v,
                o = a(t);
              return (v = s), y(t, e, o, n);
            }
            break;
          case "string":
        }
        if (null == e) return a;
        var c = e[a];
        return void 0 === c || n ? a : c;
      }
      var v,
        M = /label:\s*([^\s;\n{]+)\s*;/g;
      var R = function (t, e, a) {
          if (1 === t.length && "object" === typeof t[0] && null !== t[0] && void 0 !== t[0].styles) return t[0];
          var n = !0,
            r = "";
          v = void 0;
          var s = t[0];
          null == s || void 0 === s.raw ? ((n = !1), (r += y(a, e, s, !1))) : (r += s[0]);
          for (var o = 1; o < t.length; o++) (r += y(a, e, t[o], 46 === r.charCodeAt(r.length - 1))), n && (r += s[o]);
          M.lastIndex = 0;
          for (var c, u = ""; null !== (c = M.exec(r)); ) u += "-" + c[1];
          return {
            name: b(r) + u,
            styles: r,
            next: v,
          };
        },
        k = Object.prototype.hasOwnProperty,
        w = Object(r.createContext)("undefined" !== typeof HTMLElement ? i() : null),
        T = Object(r.createContext)({}),
        C = w.Provider,
        L = function (t) {
          var e = function (e, a) {
            return Object(r.createElement)(w.Consumer, null, function (n) {
              return t(e, n, a);
            });
          };
          return Object(r.forwardRef)(e);
        },
        A = "__EMOTION_TYPE_PLEASE_DO_NOT_USE__",
        S = function (t, e) {
          var a = {};
          for (var n in e) k.call(e, n) && (a[n] = e[n]);
          return (a[A] = t), a;
        },
        _ = function (t, e, a, n) {
          var s = null === a ? e.css : e.css(a);
          "string" === typeof s && void 0 !== t.registered[s] && (s = t.registered[s]);
          var o = e[A],
            c = [s],
            u = "";
          "string" === typeof e.className
            ? (u = h(t.registered, c, e.className))
            : null != e.className && (u = e.className + " ");
          var l = R(c);
          d(t, l, "string" === typeof o);
          u += t.key + "-" + l.name;
          var i = {};
          for (var b in e) k.call(e, b) && "css" !== b && b !== A && (i[b] = e[b]);
          return (i.ref = n), (i.className = u), Object(r.createElement)(o, i);
        },
        E = L(function (t, e, a) {
          return "function" === typeof t.css
            ? Object(r.createElement)(T.Consumer, null, function (n) {
                return _(e, t, n, a);
              })
            : _(e, t, null, a);
        });
      var N = function () {
          for (var t = arguments.length, e = new Array(t), a = 0; a < t; a++) e[a] = arguments[a];
          return R(e);
        },
        H = function (t, e) {
          var a = arguments;
          if (null == e || !k.call(e, "css")) return r.createElement.apply(void 0, a);
          var n = a.length,
            s = new Array(n);
          (s[0] = E), (s[1] = S(t, e));
          for (var o = 2; o < n; o++) s[o] = a[o];
          return r.createElement.apply(null, s);
        },
        I = L(function (t, e) {
          var a = t.styles;
          if ("function" === typeof a)
            return Object(r.createElement)(T.Consumer, null, function (t) {
              var n = R([a(t)]);
              return Object(r.createElement)(F, {
                serialized: n,
                cache: e,
              });
            });
          var n = R([a]);
          return Object(r.createElement)(F, {
            serialized: n,
            cache: e,
          });
        }),
        F = (function (t) {
          var e, a;

          function r(e, a, n) {
            return t.call(this, e, a, n) || this;
          }
          (a = t), ((e = r).prototype = Object.create(a.prototype)), (e.prototype.constructor = e), n(e, a);
          var o = r.prototype;
          return (
            (o.componentDidMount = function () {
              this.sheet = new s({
                key: this.props.cache.key + "-global",
                nonce: this.props.cache.sheet.nonce,
                container: this.props.cache.sheet.container,
              });
              var t = document.querySelector(
                "style[data-emotion-" + this.props.cache.key + '="' + this.props.serialized.name + '"]'
              );
              null !== t && this.sheet.tags.push(t),
                this.props.cache.sheet.tags.length && (this.sheet.before = this.props.cache.sheet.tags[0]),
                this.insertStyles();
            }),
            (o.componentDidUpdate = function (t) {
              t.serialized.name !== this.props.serialized.name && this.insertStyles();
            }),
            (o.insertStyles = function () {
              if (
                (void 0 !== this.props.serialized.next && d(this.props.cache, this.props.serialized.next, !0),
                this.sheet.tags.length)
              ) {
                var t = this.sheet.tags[this.sheet.tags.length - 1].nextElementSibling;
                (this.sheet.before = t), this.sheet.flush();
              }
              this.props.cache.insert("", this.props.serialized, this.sheet, !1);
            }),
            (o.componentWillUnmount = function () {
              this.sheet.flush();
            }),
            (o.render = function () {
              return null;
            }),
            r
          );
        })(r.Component),
        z = function () {
          var t = N.apply(void 0, arguments),
            e = "animation-" + t.name;
          return {
            name: e,
            styles: "@keyframes " + e + "{" + t.styles + "}",
            anim: 1,
            toString: function () {
              return "_EMO_" + this.name + "_" + this.styles + "_EMO_";
            },
          };
        },
        q = function t(e) {
          for (var a = e.length, n = 0, r = ""; n < a; n++) {
            var s = e[n];
            if (null != s) {
              var o = void 0;
              switch (typeof s) {
                case "boolean":
                  break;
                case "object":
                  if (Array.isArray(s)) o = t(s);
                  else for (var c in ((o = ""), s)) s[c] && c && (o && (o += " "), (o += c));
                  break;
                default:
                  o = s;
              }
              o && (r && (r += " "), (r += o));
            }
          }
          return r;
        };

      function B(t, e, a) {
        var n = [],
          r = h(t, n, a);
        return n.length < 2 ? a : r + e(n);
      }
      var P = L(function (t, e) {
        return Object(r.createElement)(T.Consumer, null, function (a) {
          var n = function () {
              for (var t = arguments.length, a = new Array(t), n = 0; n < t; n++) a[n] = arguments[n];
              var r = R(a, e.registered);
              return d(e, r, !1), e.key + "-" + r.name;
            },
            r = {
              css: n,
              cx: function () {
                for (var t = arguments.length, a = new Array(t), r = 0; r < t; r++) a[r] = arguments[r];
                return B(e.registered, n, q(a));
              },
              theme: a,
            },
            s = t.children(r);
          return !0, s;
        });
      });
    },
    qXWd: function (t, e) {
      t.exports = function (t) {
        if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return t;
      };
    },
    qhzo: function (t, e) {
      function a(e, n) {
        return (
          (t.exports = a =
            Object.setPrototypeOf ||
            function (t, e) {
              return (t.__proto__ = e), t;
            }),
          a(e, n)
        );
      }
      t.exports = a;
    },
    rlHP: function (t, e) {
      t.exports = function (t) {
        if ("undefined" !== typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t);
      };
    },
    tCBg: function (t, e, a) {
      var n = a("C+bE"),
        r = a("qXWd");
      t.exports = function (t, e) {
        return !e || ("object" !== n(e) && "function" !== typeof e) ? r(t) : e;
      };
    },
    tkeB: function (t, e) {
      t.exports = "_next/static/images/ban-tinh-02-c39b304271f36ff3d4685c518afa22af.png";
    },
    twyN: function (t, e) {
      t.exports = "_next/static/images/hatxanh-90f693b6b12f766cd92212bdf29c4581.png";
    },
    vlRD: function (t, e, a) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "/",
        function () {
          return a("RNiq");
        },
      ]);
    },
    ynQq: function (t, e) {
      t.exports = "_next/static/images/ban-tinh-02-63461341cbbcb1b4644e97378f57f99b.png";
    },
    "z+Cf": function (t, e, a) {
      "use strict";
      Object.defineProperty(e, "__esModule", {
        value: !0,
      });
      var n = {
        cm: !0,
        mm: !0,
        in: !0,
        px: !0,
        pt: !0,
        pc: !0,
        em: !0,
        ex: !0,
        ch: !0,
        rem: !0,
        vw: !0,
        vh: !0,
        vmin: !0,
        vmax: !0,
        "%": !0,
      };

      function r(t) {
        if ("number" === typeof t)
          return {
            value: t,
            unit: "px",
          };
        var e,
          a = t.match(/^[0-9.]*/).toString();
        e = a.includes(".") ? parseFloat(a) : parseInt(a, 10);
        var r = t.match(/[^0-9]*$/).toString();
        return n[r]
          ? {
              value: e,
              unit: r,
            }
          : (console.warn("React Spinners: " + t + " is not a valid css value. Defaulting to " + e + "px."),
            {
              value: e,
              unit: "px",
            });
      }
      (e.parseLengthAndUnit = r),
        (e.cssValue = function (t) {
          var e = r(t);
          return "" + e.value + e.unit;
        });
    },
  },
  [["vlRD", 0, 1]],
]);
